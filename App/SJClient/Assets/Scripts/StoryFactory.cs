using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class StoryFactory : MonoBehaviour
{
    public GameObject blackPanel;
    public GameObject endingPanel;

    public GameObject operationKeys;
    public GameObject chatDialog;

    public GameObject ghostPrefab;
    public GameObject caveNPCObejct;

    private MainSceneController mainSceneController;
    private PlayerController playerController;
    public static bool isProceededStory = false;

    private void Start()
    {
        mainSceneController = GameObject.FindGameObjectWithTag("Managers").GetComponent<MainSceneController>();
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
    public IEnumerator NPC_Story_1()
    {
        GameObject npc1 = GameObject.FindGameObjectWithTag("VILLAGE_NPC1");
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

        Rigidbody2D npc1Rb2D = npc1.GetComponent<Rigidbody2D>();
        Animator npc1Anim = npc1.GetComponent<Animator>();

        // story skip
        if (mainSceneController.isSkipNPCStory1) 
        {
            // NPC1 제자리 이동
            npc1.transform.localPosition = new Vector3(-15.7399998f, 19.1000004f, 0.0f);
            npc1Rb2D.constraints = RigidbodyConstraints2D.FreezeAll;
            npc1Anim.SetFloat("MoveX", -1f);
            npc1Anim.SetFloat("MoveY", 0f);

            // 콜리더들 비활성화
            GameObject.Find("/BackGround/Village/NPC/NPCStoryCollider1").GetComponent<BoxCollider2D>().enabled = false;
            GameObject.Find("/BackGround/Village/NPC").GetComponent<BoxCollider2D>().enabled = false;

            yield break;
        }

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("[ ??? ]", "“리펠드…!! 세상에. 깨어났구나. 얼마나 걱정했는 줄 아니.”"),
            new KeyValuePair<string, string>("", "낯선 이의 등장에 멈칫하다 아이 자신에 대해 아는 듯해 말을 건다."),
            new KeyValuePair<string, string>("[ ??? ]", "“누구세요…?”"),
            new KeyValuePair<string, string>("[ ??? ]", "“날 못 알아보겠니? 오, 이런. 리펠드 나란다. 너희 부모님과 친하게 지냈던 아저씨잖니.”"),
            new KeyValuePair<string, string>("", "그의 말을 통해 아이는 두 가지 사실을 알게 되었다."),
            new KeyValuePair<string, string>("", "이름이 리펠드라는 것과 그가 아이 본인을 포함하여 아이의 부모까지 알고 있다는 것이었다."),
            new KeyValuePair<string, string>("", "아이는 자신에 대한 정보를 더 알기 위해 그에게 물어보려 했으나, 남자가 먼저 입을 열었다."),
            new KeyValuePair<string, string>("[ 아저씨 ]", "“사고 이후 정신을 잃고 쓰러진 뒤로 깨어나질 않더니… 기억을 잃은 거니?”"),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“네… 아무것도 기억나지 않아요.”"),
            new KeyValuePair<string, string>("", "남자는 말했다. 아이의 부모가 사고로 인해 영영 떠나버렸고, 아이는 그로 인한 충격으로 정신을 잃고 쓰러져 한동안 일어나질 않았다고."),
            new KeyValuePair<string, string>("[ 아저씨 ]", "“걱정되어서 너네 집에 여러 번 찾아갔었단다. 지금이라도 이렇게 깨어나서 천만다행이야….”"),
            new KeyValuePair<string, string>("[ 아저씨 ]", "“부모님 일은 안타깝게 됐구나…. 참 좋은 분들이셨는데…. 부모로서도, 가족으로서도 완벽했지.”"),
            new KeyValuePair<string, string>("[ 아저씨 ]", "“행복한 가족이었는데…..”"),
            new KeyValuePair<string, string>("", "좋은 부모. 화목한 가족. 아이는 기억을 잃기 전 자신이 ‘행복’했었다는 것이 어색하게만 들렸다."),
            new KeyValuePair<string, string>("", "행복이 어떠한 감각인지 알 수 없었다. 정신을 잃고 쓰러진 영향일까. 아이는 그리 생각했다."),
            new KeyValuePair<string, string>("", "기억을 되찾으면 ‘행복’했던 감각도 다시 알 수 있을까. 아이는 남자에게 인사를 건네고 다시 길을 나섰다."),
        };


        // 플레이어 움직임 및 카메라 잠금
        camera.GetComponent<CameraController>().enabled = false;
        player.GetComponent<PlayerController>().moveStop = true;
        player.GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());
        
        // 플레이어 자리 이동
        player.transform.position = new Vector3(22.502964f, 43.3f, 0.0f);

        // 조작키 비활성화
        operationKeys.SetActive(false);

        yield return StartCoroutine(FadeOut());
        // --------------------

        // NPC1 애니메이션
        yield return StartCoroutine(NPC1Animation(npc1Rb2D, npc1Anim));

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));


        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        // 플레이어 움직임 및 카메라 잠금 해제
        camera.GetComponent<CameraController>().enabled = true;
        player.GetComponent<PlayerController>().moveStop = false;

        // NPC1 제자리 이동
        npc1.transform.localPosition = new Vector3(-15.7399998f, 19.1000004f, 0.0f);
        npc1Rb2D.constraints = RigidbodyConstraints2D.FreezeAll;
        npc1Anim.SetFloat("MoveX", -1f);
        npc1Anim.SetFloat("MoveY", 0f);

        // 콜리더들 비활성화
        GameObject.Find("/BackGround/Village/NPC/NPCStoryCollider1").GetComponent<BoxCollider2D>().enabled = false;
        GameObject.Find("/BackGround/Village/NPC").GetComponent<BoxCollider2D>().enabled = false;

        // 조작키 활성화
        operationKeys.SetActive(true);

        // NPC1 스토리 진행완료.
        playerController.sNPC1 = true;
        mainSceneController.isSkipNPCStory1 = true;

        yield return StartCoroutine(FadeOut());
        // --------------------

        yield return null;
    }

    public IEnumerator NPC_Story_2()
    {
        string objectName = "VILLAGE_NPC2_B";

        // story skip
        if (mainSceneController.isSkipNPCStory2) objectName = "VILLAGE_NPC2_A";

        List<KeyValuePair<string, string>> chatInfo = (List<KeyValuePair<string, string>>)typeof(NPC_INFO).GetField(objectName).GetValue(null);

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // NPC2 스토리 진행완료.
        playerController.sNPC2 = true;
        mainSceneController.isSkipNPCStory2 = true;

        yield return null;
    }

    public IEnumerator NPC_Story_3()
    {
        string objectName = "VILLAGE_NPC3_B";

        // story skip
        if (mainSceneController.isSkipNPCStory3) objectName = "VILLAGE_NPC3_A";

        List<KeyValuePair<string, string>> chatInfo = (List<KeyValuePair<string, string>>)typeof(NPC_INFO).GetField(objectName).GetValue(null);

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // NPC3 스토리 진행완료.
        playerController.sNPC3 = true;
        mainSceneController.isSkipNPCStory3 = true;

        yield return null;
    }

    public IEnumerator Ghost_Story(Vector3 pos)
    {
        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider1").GetComponent<BoxCollider2D>().enabled = false;
        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider2").GetComponent<BoxCollider2D>().enabled = false;

        // story skip
        if (mainSceneController.isSkipCaveExit) yield break;

        GameObject ghostObj = Instantiate(ghostPrefab, caveNPCObejct.transform, false);
        ghostObj.transform.position = pos;

        yield return StartCoroutine(GhostFadeIn(ghostObj));

        ghostObj.GetComponent<GhostAIManager>().startMove = true;
        BoxCollider2D[] ghostColiders = ghostObj.GetComponents<BoxCollider2D>();
        for (int i = 0; i < ghostColiders.Length; i++)
            ghostColiders[i].enabled = true;

        yield return null;
    }

    public IEnumerator Cave_Exit_Story()
    {
        GameObject.Find("/BackGround/Cave/NPC/CaveExitStoryCollider").GetComponent<BoxCollider2D>().enabled = false;

        // story skip
        if (mainSceneController.isSkipCaveExit) yield break;

        GameObject.FindGameObjectWithTag("Ghost").GetComponent<GhostAIManager>().startMove = false;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("", "막다른 길이다. 앞으로 나아갈 수 없다."),
            new KeyValuePair<string, string>("", "방금 전까지 검은 안개가 쫓아와 뒤돌아 갈 수 없다. 아이는 이도저도 못하는 상황에 눈을 질끔 감는다."),
            new KeyValuePair<string, string>("", "하지만 시간이 지나도 아무 일이 일어나지 않는다."),
            new KeyValuePair<string, string>("", "주변을 둘러보던 중 아이는 익숙한 풍경임을 눈치챈다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“여기…. 예전에도 왔었던 거 같아.”"),
            new KeyValuePair<string, string>("", "호기심에 들어왔던 기억을 되새긴다. 그때도 지금처럼 동굴 안으로 들어올 수 있도록 되어 있었다. 다른 점이 있다면 예전엔 검은 안개가 쫓아오지 않았다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“검은 안개는… 대체 뭘까..?”"),
            new KeyValuePair<string, string>("", "계속 생각해보지만 답은 나오지 않는다. 그저 머리가 지끈거릴 뿐이다. 아이는 이곳에서의 기억을 되짚어보기로 한다."),
            new KeyValuePair<string, string>("", "부모님이 해줬던 말을 아이는 기억해낸다. 웃으면서 동굴 안에 엄청나게 좋은 것이 있다는 말을 했었다. 무엇인지는 명확하게 생각나진 않는다."),
            new KeyValuePair<string, string>("", "그래도 아이는 부모님의 말을 듣고 동굴 안으로 들어와 ‘좋은 것’을 찾아다녔다. 부모님께 주고픈 마음이 컸던 탓에 어두운 동굴 안으로 들어갔었다."),
            new KeyValuePair<string, string>("", "그리고 기억은 여기서 끊겼다. 동굴에서 무슨 일이 있었는지는 모르지만, 그저 아빠가 아이를 데리러 왔고 함께 밖으로 나갔던 기억이 전부이다."),
            new KeyValuePair<string, string>("", "아이는 아빠와 손을 잡고 밖을 나갔던 기억을 마지막으로 더 이상 떠오르지 않는다. 딱 한 가지 더 알아낸 것은 기분이 싫다기보다 좋았던 쪽에 가까운 것이다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“밖으로 나가는 길이… 기억나. 아빠랑 같이 나갔었어.”"),
            new KeyValuePair<string, string>("", "아이는 머릿속에 남은 기억대로 밖을 향해 걸음을 옮긴다."),
        };

        player.GetComponent<PlayerController>().moveStop = true;
        player.GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        // 악몽 제거
        Destroy(GameObject.FindGameObjectWithTag("Ghost"));

        // 조작키 비활성화
        operationKeys.SetActive(false);

        yield return StartCoroutine(FadeOut());

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        player.transform.position = new Vector3(41f, 51.2f, 0f);

        camera.GetComponent<CameraController>().limitFlag = true;
        player.GetComponent<PlayerController>().anim.SetFloat("MoveX", 0f);
        player.GetComponent<PlayerController>().anim.SetFloat("MoveY", -1f);
        player.GetComponent<PlayerController>().moveStop = false;

        // 조작키 활성화
        operationKeys.SetActive(true);

        // Exit 스토리 진행완료.
        playerController.sExit = true;
        mainSceneController.isSkipCaveExit = true;

        // 행복게이지 증가
        playerController.happy_gauge = playerController.happy_gauge + 10 > 100 ? 100 : playerController.happy_gauge + 10;
        playerController.operationKeys.transform.Find("Happiness Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = playerController.happy_gauge / 100.0f;

        yield return StartCoroutine(FadeOut());

        // 만약 행복게이지가 100이라면 첫번째 엔딩을 타도록 하자.
        if (playerController.happy_gauge == 100)
            yield return StartCoroutine(this.The_First_Ending_Story());

        yield return null;
    }

    public IEnumerator Ghost_Catched_Story(GameObject gameObj)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        GameObject bed = GameObject.FindGameObjectWithTag("Bed");

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("[ 리펠드 ]", "“내가 왜 여기… 왜 침대에….?”"),
            new KeyValuePair<string, string>("", "잠에서 깨어난 듯 아이는 침대에서 일어났다."),
            new KeyValuePair<string, string>("", "악몽이라도 꾼 마냥 방금 전의 일이 꿈같기도 하고 현실 같기도 하다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“대체 뭘까.”"),
        };

        player.GetComponent<PlayerController>().moveStop = true;
        player.GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        // 악몽 제거
        Destroy(gameObj);

        // 되돌리자
        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider1").GetComponent<BoxCollider2D>().enabled = true;
        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider2").GetComponent<BoxCollider2D>().enabled = true;

        // 플레이어 자리 이동
        bed.GetComponent<BoxCollider2D>().enabled = false;
        camera.GetComponent<CameraController>().enabled = false;
        player.transform.position = new Vector3(-1.95f, 0.9f, 0.0f);
        camera.transform.position = new Vector3(0.0f, 0.0f, -10.0f);

        player.GetComponent<PlayerController>().anim.SetFloat("MoveX", 0f);
        player.GetComponent<PlayerController>().anim.SetFloat("MoveY", -1f);

        // 조작키 비활성화
        operationKeys.SetActive(false);

        yield return StartCoroutine(FadeForGhostStory(chatInfo));
    }

    public IEnumerator CAVE_Story()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        // story skip
        if (mainSceneController.isSkipCaveStory)
        {
            // 페이드 인
            yield return StartCoroutine(FadeIn());

            // 카메라 제한 끄기
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().limitFlag = false;

            // 플레이어 자리 이동
            player.transform.position = new Vector3(73.3848419f, 37.0568581f, 0);

            // 페이드 아웃
            yield return StartCoroutine(FadeOut());

            yield break;
        }

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("", "동굴 입구가 처음엔 나무판자로 막혀져 있다가 지금은 무너져 내려 안으로 출입이 가능해 보인다."),
            new KeyValuePair<string, string>("", "앞에 놓은 돌덩이 정도는 넘을 수 있을 것 같다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“표지판에는 들어가지 말라고 적혀 있는데.”"),
            new KeyValuePair<string, string>("", "아이는 돌덩이 앞에 서서 잠시 갈등한다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“그래도… 왠지 안에 들어가면 뭔가를 알아낼 수 있을 거 같은 느낌이 들어.”"),
            new KeyValuePair<string, string>("", "아이는 기억을 되찾기 위해 돌덩이를 넘어간다."),
        };

        // 조작키 비활성화
        operationKeys.SetActive(false);

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // 페이드 인
        yield return StartCoroutine(FadeIn());

        // 카메라 제한 끄기
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().limitFlag = false;

        // 플레이어 자리 이동
        player.transform.position = new Vector3(73.3848419f, 37.0568581f, 0);

        // 조작키 활성화
        operationKeys.SetActive(true);

        // CAVE 스토리 진행완료.
        playerController.sCave = true;
        mainSceneController.isSkipCaveStory = true;

        // 페이드 아웃
        yield return StartCoroutine(FadeOut());
        // --------------------

        yield return null;
    }

    public IEnumerator The_First_Ending_Story()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("", "아이는 눈물을 흘린다. 되찾은 기억을 바탕으로 자신이 겪어온 기억을 다시금 되짚어 본다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“부모님은 날 많이 사랑하셨구나.”"),
            new KeyValuePair<string, string>("", "이상적인 가족의 모습. 아이를 많이 사랑하는 부모의 모습. 아이는 중얼거린다. 밖에서의 모습은 그러했다."),
            new KeyValuePair<string, string>("", "부모의 사랑을 받고 자랐음을 인지하자 그들의 죽음에 대해 아이는 슬퍼한다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“……”"),
            new KeyValuePair<string, string>("", "아이는 집안을 둘러보다 이내 이상함을 느낀다. 어째서인지 이 집에서의 기억이 가물가물하다. 밖에서 얼핏 찾은 기억만으로는 애매했다. 제대로 된 집에서의 기억이 없었다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“처음 여기서 일어났을 때랑… 달라진 게 없는 거 같아.”"),
            new KeyValuePair<string, string>("", "밖에서 기억과 감정을 되찾았음에도 불구하고 이 집 안에서의 기억이 없다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“밖에서 어느 정도 우리 집 얘기를 들었으니까 쉽게 떠올릴 수 있지 않을까..?”"),
            new KeyValuePair<string, string>("", "아이는 이곳, 이 집안이 부모님과의 기억으로 가득할 테니 그들과의 기억을 되찾고 그에 따른 감정을 느끼고자 집안을 다시금 살펴본다."),
        };

        player.GetComponent<PlayerController>().moveStop = true;
        player.GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        // 방으로 이동
        player.transform.position = new Vector3(0.37f, -59.84f, 0.0f);

        camera.GetComponent<CameraController>().limitFlag = false;
        player.GetComponent<PlayerController>().anim.SetFloat("MoveX", 0f);
        player.GetComponent<PlayerController>().anim.SetFloat("MoveY", -1f);

        // 조작키 비활성화
        operationKeys.SetActive(false);

        yield return StartCoroutine(FadeOut());

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // 조작키 활성화
        operationKeys.SetActive(true);
        player.GetComponent<PlayerController>().moveStop = false;

        yield return null;
    }

    public IEnumerator The_Last_Ending_Story()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

        List<KeyValuePair<string, string>> chatInfo = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("", "아이는 아무도 없는 침대를 바라본다. 이젠 뚜렷해진 부모의 모습을 떠올리며 기억들을 하나씩 되짚기 시작한다."),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“진심이 아니었구나.”"),
            new KeyValuePair<string, string>("", "한편으로는 아이가 부모의 사랑이 진실이라고 자기 자신에게 최면을 건 것은 아닐까 생각한다."),
            new KeyValuePair<string, string>("", "그만큼 아이 자신도 자각을 하고 있었다. 단지 착각을 하고 싶어서, 그렇게 믿고 싶어서 믿은 것 뿐이었다. 그래야 이 고통을 견딜 수 있으니까."),
            new KeyValuePair<string, string>("", "아이는 저를 향하던 부모의 얼굴과 거기서 알 수 있는 감정을 떠올리며 눈을 감는다. 눈에 맺혀 있던 눈물이 눈꺼풀이 내려옴에 따라 눈가로 흘러내린다."),
            new KeyValuePair<string, string>("", "마치 깊은 잠에서 깨어난 마냥 아이는 눈을 뜬다. "),
            new KeyValuePair<string, string>("[ 리펠드 ]", "“부모님은 그냥 날 싫어해서… 그런 거였어.”"),
            new KeyValuePair<string, string>("", "최면을 푼 아이는 사랑이 없었음을 인정한다. 그렇게 생각을 뒤바꾸자, 꽉 붙잡고 있던 줄을 놓은 마냥 어딘가 마음 한켠이 홀가분해진다."),
            new KeyValuePair<string, string>("", "아이는 옅은 미소를 띤 채 침대에서 돌아선다. 그리고 미처 인사하지 못한 그곳으로 가 마지막 인사를 전하러 발걸음을 옮긴다."),
        };

        player.GetComponent<PlayerController>().moveStop = true;
        player.GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);

        // --- 페이드 인 & 아웃
        yield return StartCoroutine(FadeIn());

        // 부모님 방으로 이동
        player.transform.position = new Vector3(58.66f, -58.9f, 0.0f);

        camera.GetComponent<CameraController>().limitFlag = false;
        player.GetComponent<PlayerController>().anim.SetFloat("MoveX", 1f);
        player.GetComponent<PlayerController>().anim.SetFloat("MoveY", 0f);

        // 조작키 비활성화
        operationKeys.SetActive(false);

        yield return StartCoroutine(FadeOut());

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));


        yield return StartCoroutine(FadeForLastEnding());


        // 인트로로 가자
        SceneManager.LoadScene("IntroScene");
    }
    private IEnumerator GhostFadeIn(GameObject ghostObj)
    {
        float fadeCount = 0;

        ghostObj.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, fadeCount);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.02f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            ghostObj.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, fadeCount);
        }
    }

    private IEnumerator NPC1Animation(Rigidbody2D npc1Rb2D, Animator npc1Anim)
    {
        float moveDistance = 5.0f;
        float movedX = 0.0f;

        while (movedX < moveDistance)
        {
            movedX += 0.05f;
            yield return new WaitForSeconds(0.01f); // 0.1초마다
            npc1Rb2D.MovePosition(npc1Rb2D.position + new Vector2(1f, 0f).normalized * 2 * Time.fixedDeltaTime);

            npc1Anim.SetBool("NPC1Moving", true);
            npc1Anim.SetFloat("MoveX", 1f);
        }

        npc1Anim.SetBool("NPC1Moving", false);
    }

    private IEnumerator OnChatDialog(List<KeyValuePair<string, string>> chatInfoDic)
    {
        if (chatInfoDic == null || chatInfoDic.Count == 0) yield return null;

        // 채팅창을 켜보자 
        chatDialog.SetActive(true);

        GameObject chatPanel = chatDialog.transform.GetChild(0).gameObject;
        EventTrigger trigger;
        EventTrigger.Entry entry;
        bool NextFlag = false;

        for (int i = 0; i < chatInfoDic.Count; i++)
        {
            chatPanel.transform.Find("Name_Text").GetComponent<Text>().text = chatInfoDic[i].Key;
            chatPanel.transform.Find("Chat_Text").GetComponent<Text>().text = chatInfoDic[i].Value;

            // 버튼 이벤트 추가
            trigger = chatPanel.transform.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { NextFlag = true; });

            trigger.triggers.RemoveAll(x => true);
            trigger.triggers.Add(entry);

            // 창이 닫힐 때까지 대기한다.
            yield return new WaitUntil(() => NextFlag);
            NextFlag = false;
        }

        // 채팅창을 끄자
        chatDialog.SetActive(false);
    }

    private IEnumerator FadeIn()
    {
        float fadeCount = 0;

        blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);
        blackPanel.SetActive(true);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }
    }

    private IEnumerator FadeOut()
    {
        float fadeCount = 1f;
        blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);

        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        blackPanel.SetActive(false);
    }

    private IEnumerator FadeForGhostStory(List<KeyValuePair<string, string>> chatInfo)
    {
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        float fadeCount = 1f;
        blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);

        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        blackPanel.SetActive(false);

        fadeCount = 0;

        blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);
        blackPanel.SetActive(true);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.02f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        //////////////////////////////////////////////////////////////////////////////////////////////

        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(-1.0f, 0.5f, 0.0f);
        GameObject.FindGameObjectWithTag("Bed").GetComponent<BoxCollider2D>().enabled = true;

        //////////////////////////////////////////////////////////////////////////////////////////////

        fadeCount = 1f;

        blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);

        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.02f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        blackPanel.SetActive(false);

        //////////////////////////////////////////////////////////////////////////////////////////////

        // 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo));

        // 조작키 활성화 
        operationKeys.SetActive(true);

        // 카메라 설정 및 움직임 잠금 해제
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().enabled = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().moveStop = false;

    }

    private IEnumerator FadeForLastEnding()
    {
        float fadeCount = 0;

        GameObject endText = endingPanel.transform.GetChild(0).gameObject;
        GameObject etcText = endingPanel.transform.GetChild(1).gameObject;

        //======= 검은 화면 등장

        endingPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount);
        endingPanel.SetActive(true);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            endingPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        //======= END

        fadeCount = 0;

        endText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount);
        endText.SetActive(true);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            endText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        yield return new WaitForSeconds(2f); // 2초 유지 

        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            endText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        yield return new WaitForSeconds(1f); // 1초 유지 

        //======= etc

        fadeCount = 0;

        etcText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount);
        etcText.SetActive(true);

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            etcText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        yield return new WaitForSeconds(2f); // 2초 유지 

        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            etcText.GetComponent<Text>().color = new Color(255, 255, 255, fadeCount); // 해당 변수 값으로 알파 값 지정
        }
    }
}