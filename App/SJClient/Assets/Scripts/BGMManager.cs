using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : MonoBehaviour
{
    public static BGMManager instance;

    private AudioSource audiosrc;
    private bool bPaused;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        audiosrc = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            bPaused = true;
            audiosrc.Pause();
        }
        else
        {
            if (bPaused)
            {
                bPaused = false;
                audiosrc.Play();
            }
        }
    }
}
