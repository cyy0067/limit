using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    Transform tr;

    public bool limitFlag = false;

    public float limitMinX;
    public float limitMinY;

    public float limitMaxX;
    public float limitMaxY;

    // Start is called before the first frame update
    void Start()
    {
        tr = player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float x, y;
        if (limitFlag)
        {
            x = Mathf.Clamp(tr.position.x, limitMinX, limitMaxX);
            y = Mathf.Clamp(tr.position.y, limitMinY, limitMaxY);
        }
        else
        {
            x = tr.position.x;
            y = tr.position.y;
        }
        transform.position = new Vector3(x, y, -10);
    }
}
