using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonDataController : MonoBehaviour
{
    string path;

    void Start()
    {
        path = Path.Combine(Application.dataPath + "/Data/", "database.json");
        JsonLoad();
    }

    public void JsonLoad() 
    {
        SaveDataList saveDataList = new SaveDataList();

        string loadJson = @"
        {
            ""save_data_list"": [
              {
                ""index"": 1,
                ""player_pos_x"": 26.66315,
                ""player_pos_y"": 43.57315,
                ""camera_limit"": true,
                ""happy_gauge"": 99,
                ""depress_gauge"": 99,
                ""memory_history_list"": [ false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ],
                ""afed_history_list"": [ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ],
                ""sPrologue"": true,
                ""sNPC1"": false,
                ""sNPC2"": true,
                ""sNPC3"": false,
                ""sCave"": true,
                ""sExit"": false,
                ""start_time"": 0.0,
                ""end_time"": 0.0,
                ""start_date"": ""2022/XX/XX""
              }
            ]
        }";

        if (File.Exists(path)) loadJson = File.ReadAllText(path);

        saveDataList = JsonUtility.FromJson<SaveDataList>(loadJson);

        for (int i = 0; i < saveDataList.save_data_list.Count; i++)
        {
            SaveDataManager.instance.SaveDataList.Add(saveDataList.save_data_list[i]);
        }
    }

    public void JsonSave()
    {
        SaveDataList saveDataList = new SaveDataList();

        for (int i = 0; i < SaveDataManager.instance.SaveDataList.Count; i++)
        {
            saveDataList.save_data_list.Add(SaveDataManager.instance.SaveDataList[i]);
        }

        string json = JsonUtility.ToJson(saveDataList, true);
        File.WriteAllText(path, json);
    }
}
