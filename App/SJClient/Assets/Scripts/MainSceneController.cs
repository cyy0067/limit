using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class MainSceneController : MonoBehaviour
{
    // 저장 파일에 상태값으로 존재해야 한다.
    public bool isSkipPrologue = false;
    public bool isSkipNPCStory1 = false;
    public bool isSkipNPCStory2 = false;
    public bool isSkipNPCStory3 = false;
    public bool isSkipCaveStory = false;
    public bool isSkipCaveExit = false;

    public GameObject menuPanel;
    public GameObject savePanel;
    public GameObject blackPanel;

    public GameObject operationKeys;
    public GameObject chatDialog;
    public GameObject tutorialDialog;
    public GameObject diaryDialog;
    public GameObject collectDialog;

    PlayerController playerController;

    private void Awake()
    {
        //StartCoroutine(FadeOut());
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.startTime = Time.time;

        // 로드된 메타데이터가 존재한다면, 메타데이터를 Set 하자.
        if (SaveDataManager.instance && SaveDataManager.instance.LoadedMetaData.index != -1)
        { 
            SaveData loadData = SaveDataManager.instance.LoadedMetaData;

            if (loadData.sPrologue)  isSkipPrologue  = true;
            if (loadData.sNPC1)      isSkipNPCStory1 = true;
            if (loadData.sNPC2)      isSkipNPCStory2 = true;
            if (loadData.sNPC3)      isSkipNPCStory3 = true;
            if (loadData.sCave)      isSkipCaveStory = true;
            if (loadData.sExit)      isSkipCaveExit  = true;

            playerController.memory_history_list = loadData.memory_history_list;
            playerController.afed_history_list = loadData.afed_history_list;
            playerController.sPrologue = isSkipPrologue;
            playerController.sNPC1 = isSkipNPCStory1;
            playerController.sNPC2 = isSkipNPCStory2;
            playerController.sNPC3 = isSkipNPCStory3;
            playerController.sCave = isSkipCaveStory;
            playerController.sExit = isSkipCaveExit;

            playerController.happy_gauge = loadData.happy_gauge;
            playerController.depress_gauge = loadData.depress_gauge;
            playerController.startTime = loadData.start_time;

            // 게이지 UI 반영하기
            playerController.operationKeys.transform.Find("Depression Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = playerController.depress_gauge / 100.0f;
            playerController.operationKeys.transform.Find("Happiness Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = playerController.happy_gauge / 100.0f;

            GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(loadData.player_pos_x, loadData.player_pos_y, 0.0f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().limitFlag = loadData.camera_limit;

            // npc1 story skip
            if (isSkipNPCStory1)
            {
                GameObject npc1 = GameObject.FindGameObjectWithTag("VILLAGE_NPC1");
                Rigidbody2D npc1Rb2D = npc1.GetComponent<Rigidbody2D>();
                Animator npc1Anim = npc1.GetComponent<Animator>();

                // NPC1 占쏙옙占쌘몌옙 占싱듸옙
                npc1.transform.localPosition = new Vector3(-15.7399998f, 19.1000004f, 0.0f);
                npc1Rb2D.constraints = RigidbodyConstraints2D.FreezeAll;
                npc1Anim.SetFloat("MoveX", -1f);
                npc1Anim.SetFloat("MoveY", 0f);

                // 占쌥몌옙占쏙옙占쏙옙 占쏙옙활占쏙옙화
                GameObject.Find("/BackGround/Village/NPC/NPCStoryCollider1").GetComponent<BoxCollider2D>().enabled = false;
                GameObject.Find("/BackGround/Village/NPC").GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }

    private void Start()
    {
        // 프롤로그를 스킵한 경우, 주인공 위치 세팅
        if (isSkipPrologue)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().enabled = true;
            GameObject.FindGameObjectWithTag("Bed").GetComponent<BoxCollider2D>().enabled = true;

            if (!SaveDataManager.instance || SaveDataManager.instance.LoadedMetaData.index == -1)
            {
                // - 침대 옆
                GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(0.075f, 0.45f, 0.0f);
                // - 마을
                //GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(24.0230007f, 43.3839989f, 0.0f);
                // - 동굴
                //GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(41f, 51.2f, 0.0f);
                // - 1차 엔딩 이후
                //GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(0.075f, -59.55f, 0.0f);
            }

            operationKeys.SetActive(true);
        }
        // 프롤로그를 스킵하지 않은 경우, 프롤로그 진행
        else
        {
            StartCoroutine(Prologue());
        }
    }

    public void OpenMenuPanel()
    {
        menuPanel.SetActive(true);
    }

    public void CloseMenuPanel()
    {
        menuPanel.SetActive(false);
    }

    public void OpenSavePanel()
    {
        savePanel.SetActive(true);

        // 오브젝트에 데이터를 로드한다.
        SaveDataManager.instance.LoadToObject();
    }

    public void CloseSavePanel()
    {
        savePanel.SetActive(false);
    }

    public void ChangeSceneToIntro()
    {
        StartCoroutine(FadeInAndLoadMainScene());
    }

    private IEnumerator Prologue()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        GameObject bed = GameObject.FindGameObjectWithTag("Bed");

        List<KeyValuePair<string, string>> chatInfo1 = new List<KeyValuePair<string, string>>()
        {
             new KeyValuePair<string, string>("[ ??? ]", "\"……\""),
             new KeyValuePair<string, string>("[ ??? ]", "\"……?\""),
             new KeyValuePair<string, string>("[ ??? ]", "\"여긴 어디지?\""),
             new KeyValuePair<string, string>("[ ??? ]", "\"나. … 이름도 생각이 안 나.\""),
             new KeyValuePair<string, string>("", "그때 머리가 지끈거리기 시작했다."),
        };
        List<KeyValuePair<string, string>> chatInfo2 = new List<KeyValuePair<string, string>>()
        {
             new KeyValuePair<string, string>("", "고통과 함께 점점 무언가가 머릿속에 떠올려지기 시작했다."),
             new KeyValuePair<string, string>("", "손바닥을 감싸는 뜨겁고 축축한 감각이 번뜩였다."),
             new KeyValuePair<string, string>("", "그러나 그것뿐, 그 외 알게 된 건 없었다."),
             new KeyValuePair<string, string>("", "아이는 궁금해졌다."),
             new KeyValuePair<string, string>("", "방금 느낀 그 감각은 무엇이며, 어째서 그걸 느끼고도 드는 감정이 없는 것인지."),
             new KeyValuePair<string, string>("", "아이는 자신에 대해 이상함을 느꼈다."),
             new KeyValuePair<string, string>("[ ??? ]", "\"…이 주변을 둘러보면 무엇이라도 알 수 있지 않을까?\""),
             new KeyValuePair<string, string>("", "아이는 자신에 대한 기억을 되찾는다면 그때 느낀 감각이 무엇인지 알 수 있지 않을까 생각했다."),
             new KeyValuePair<string, string>("", "아이는 지금 가만히 있는 것보다 움직이는 것이 옳다고 판단했다."),
        };

        // 플레이어 움직임 잠금
        player.GetComponent<PlayerController>().moveStop = true;

        // 침대에서 눈 깜빡
        yield return StartCoroutine(FadeOut());
        yield return new WaitForSeconds(1f);

        // 침대 옆으로 오도록
        yield return StartCoroutine(FadeIn());
        player.transform.position = new Vector3(-1.0f, 0.5f, 0.0f);
        bed.GetComponent<BoxCollider2D>().enabled = true;
        yield return StartCoroutine(FadeOut());
        yield return new WaitForSeconds(0.5f);

        // 첫번째 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo1));

        // 세번 깜빡
        yield return StartCoroutine(FadeIn());
        yield return StartCoroutine(FadeOut());
        yield return StartCoroutine(FadeIn());
        yield return StartCoroutine(FadeOut());
        yield return StartCoroutine(FadeIn());
        yield return StartCoroutine(FadeOut());

        // 두번째 대화창
        yield return StartCoroutine(OnChatDialog(chatInfo2));

        // 게임 설명창
        tutorialDialog.SetActive(true);

        GameObject gameInfoPanel = tutorialDialog.transform.GetChild(0).gameObject;
        gameInfoPanel.SetActive(true);

        EventTrigger trigger = gameInfoPanel.transform.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        bool NextFlag = false;
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { NextFlag = true; });

        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        yield return new WaitUntil(() => NextFlag);
        gameInfoPanel.SetActive(false);
        NextFlag = false;

        // 조작키 설명창
        GameObject tutorialPanel = tutorialDialog.transform.GetChild(1).gameObject;
        tutorialPanel.SetActive(true);
        operationKeys.SetActive(true);

        trigger = tutorialPanel.transform.GetComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { NextFlag = true; });

        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        yield return new WaitUntil(() => NextFlag);
        tutorialPanel.SetActive(false);

        // 카메라 설정 및 움직임 잠금 해제
        camera.GetComponent<CameraController>().enabled = true;
        player.GetComponent<PlayerController>().moveStop = false;

        // 프롤로그 진행완료.
        isSkipPrologue = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().sPrologue = true;

    }

    public IEnumerator OnChatDialog(List<KeyValuePair<string, string>> chatInfoDic)
    {
        if (chatInfoDic == null || chatInfoDic.Count == 0) yield return null;

        // 채팅창을 켜보자 
        chatDialog.SetActive(true);

        GameObject chatPanel = chatDialog.transform.GetChild(0).gameObject;
        EventTrigger trigger;
        EventTrigger.Entry entry;
        bool NextFlag = false;

        for (int i = 0; i < chatInfoDic.Count; i++)
        {
            chatPanel.transform.Find("Name_Text").GetComponent<Text>().text = chatInfoDic[i].Key;
            chatPanel.transform.Find("Chat_Text").GetComponent<Text>().text = chatInfoDic[i].Value;

            // 버튼 이벤트 추가
            trigger = chatPanel.transform.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { NextFlag = true; });

            trigger.triggers.RemoveAll(x => true);
            trigger.triggers.Add(entry);

            // 창이 닫힐 때까지 대기한다.
            yield return new WaitUntil(() => NextFlag);
            NextFlag = false;
        }

        // 채팅창을 끄자
        chatDialog.SetActive(false);
    }

    public IEnumerator OnDiaryDialog(List<KeyValuePair<string, string>> diaryInfoDic)
    {
        if (diaryInfoDic == null || diaryInfoDic.Count == 0) yield return null;

        GameObject diaryPanel = diaryDialog.transform.GetChild(0).gameObject;
        EventTrigger trigger;
        EventTrigger.Entry entry;
        bool NextFlag = false;

        diaryPanel.transform.Find("Title").GetComponent<Text>().text = diaryInfoDic[4].Value;
        diaryPanel.transform.Find("Contents").GetComponent<Text>().text = diaryInfoDic[5].Value;

        // 일기장을 켜보자 
        diaryDialog.SetActive(true);

        // 버튼 이벤트 추가
        trigger = diaryPanel.transform.GetChild(0).gameObject.GetComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { NextFlag = true; });

        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        // 창이 닫힐 때까지 대기한다.
        yield return new WaitUntil(() => NextFlag);

        Int16 index = Convert.ToInt16(diaryInfoDic[0].Value);
        bool isHappiness = Convert.ToBoolean(diaryInfoDic[2].Value);
        Int16 gaugeValue = Convert.ToInt16(diaryInfoDic[3].Value);

        // 기억 히스토리 갱신
        playerController.memory_history_list[index] = true;

        // 행복도 및 우울도 증가
        if (isHappiness) playerController.happy_gauge = playerController.happy_gauge + gaugeValue > 100 ? 100 : playerController.happy_gauge + gaugeValue;
        else playerController.depress_gauge += playerController.depress_gauge + gaugeValue > 100 ? 100 : playerController.depress_gauge + gaugeValue;

        // 일기장을 끄자 
        diaryDialog.SetActive(false);
    }

    public IEnumerator OnCollectDialog(List<KeyValuePair<string, string>> diaryInfoDic)
    {
        if (diaryInfoDic == null || diaryInfoDic.Count == 0) yield return null;

        GameObject collectPanel = collectDialog.transform.GetChild(0).gameObject;
        collectPanel.transform.Find("Info").GetComponent<Text>().text = "감정 [" + diaryInfoDic[1].Value + "]을 되찾았습니다.";

        // 감정창을 켜보자 
        collectDialog.SetActive(true);
        byte fadeCount = 255;

        while (fadeCount > 0)
        {
            fadeCount -= 1;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            collectDialog.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(240, 240, 240, fadeCount); // 해당 변수 값으로 알파 값 지정
            collectDialog.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color32(50, 50, 50, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        collectDialog.SetActive(false);

        // 초기화
        collectDialog.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(240, 240, 240, 255);
        collectDialog.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color32(50, 50, 50, 255);

        // 게이지 UI 반영하기
        playerController.operationKeys.transform.Find("Depression Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = playerController.depress_gauge / 100.0f;
        playerController.operationKeys.transform.Find("Happiness Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = playerController.happy_gauge / 100.0f;
    }

    public IEnumerator FadeIn()
    {
        blackPanel.SetActive(true);

        float fadeCount = 0;

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }
    }

    public IEnumerator FadeOut()
    {
        blackPanel.SetActive(true);

        float fadeCount = 1f;
        
        while (fadeCount > 0.0f)
        {
            fadeCount -= 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        blackPanel.SetActive(false);
    }

    private IEnumerator FadeInAndLoadMainScene()
    {
        blackPanel.SetActive(true);

        float fadeCount = 0;

        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.GetComponent<Image>().color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        SceneManager.LoadScene("IntroScene");
    }
}
