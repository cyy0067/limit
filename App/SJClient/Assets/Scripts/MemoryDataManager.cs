using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System;
using System.Collections;
using System.Collections.Generic;

using System.Reflection;
using System.ComponentModel.Design;
using System.Linq;

public class MemoryDataManager : MonoBehaviour
{
    PlayerController playerController;
    public GameObject memoryPanel;

    public List<List<KeyValuePair<string, string>>> MEMORY_INFO_LIST = new List<List<KeyValuePair<string, string>>>();

    void Awake()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        // 기억 정보 저장
        List<FieldInfo> fieldList = new List<FieldInfo>();
        fieldList.AddRange(typeof(MCR_INFO).GetFields());
        fieldList.AddRange(typeof(LR_INFO).GetFields());
        fieldList.AddRange(typeof(PR_INFO).GetFields());
        fieldList.AddRange(typeof(VG_INFO).GetFields());
        fieldList.AddRange(typeof(H1_INFO).GetFields());
        fieldList.AddRange(typeof(H3_INFO).GetFields());
        fieldList.AddRange(typeof(H5_INFO).GetFields());
        fieldList.AddRange(typeof(H6_INFO).GetFields());

        FieldInfo[] fieldInfo = fieldList.ToArray();

        for (int i = 0; i < fieldInfo.Length; i++)
        {
            string objectName = fieldInfo[i].Name;
            if ((objectName.Length > 2 && objectName.Substring(objectName.Length - 2).Equals("MM")))
                MEMORY_INFO_LIST.Add((List<KeyValuePair<string, string>>)fieldInfo[i].GetValue(null));
        }

        // index 순서대로 sorting 한다.
        MEMORY_INFO_LIST.Sort(
            delegate (List<KeyValuePair<string, string>> c1, List<KeyValuePair<string, string>> c2) { 
                return Convert.ToInt16(c1[0].Value).CompareTo(Convert.ToInt16(c2[0].Value)); 
            });
    }

    public void LoadToObject()
    {
        GameObject memoryDataContent = GameObject.FindGameObjectWithTag("MemoryDataContent");

        // 기존 데이터 리셋
        foreach (Transform trans in memoryDataContent.transform)
        {
            EventTrigger trigger = trans.GetChild(0).gameObject.GetComponent<EventTrigger>();
            trigger.triggers.RemoveAll(x => true);

            trans.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
            trans.GetChild(0).Find("Context").GetComponent<Text>().text = "";
        }

        // 로드된 데이터로 채운다.
        for (int i = 0; i < this.playerController.memory_history_list.Count; i++)
        {
            // false면 패스
            if (!this.playerController.memory_history_list[i]) continue;
        
            Transform loadData = memoryDataContent.transform.GetChild(i).GetChild(0);
            EventTrigger trigger = loadData.gameObject.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();

            List<KeyValuePair<string, string>> memoryInfo = MEMORY_INFO_LIST[i];

            string emotion = memoryInfo[1].Value;
            string title = memoryInfo[4].Value;
            string context = "[" + emotion + "] : " + title;

            loadData.gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
            loadData.Find("Context").GetComponent<Text>().text = context;

            // 저장하기 이벤트 추가
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { this.OpenMemoryInfo(memoryInfo); });
            trigger.triggers.Add(entry);
        }
    }

    public void OpenMemoryInfo(List<KeyValuePair<string, string>> diaryInfo)
    {
        GameObject diaryDialog = GameObject.FindWithTag("MemoryPanel").transform.Find("Diary Dialog").gameObject;

        diaryDialog.transform.GetChild(0).Find("Title").GetComponent<Text>().text = diaryInfo[4].Value;
        diaryDialog.transform.GetChild(0).Find("Contents").GetComponent<Text>().text = diaryInfo[5].Value;

        // 일기창 열기
        diaryDialog.SetActive(true);
    }

    public void CloseMemoryInfo()
    {
        GameObject diaryDialog = GameObject.FindWithTag("MemoryPanel").transform.Find("Diary Dialog").gameObject;
        
        // 일기창 닫기
        diaryDialog.SetActive(false);
    }
}
