using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject loadPanel;
    public Image blackPanel;

    public void ChangePanelToMain()
    {
        mainPanel.SetActive(true);
        loadPanel.SetActive(false);
    }

    public void ChangePanelToLoad()
    {
        mainPanel.SetActive(false);
        loadPanel.SetActive(true);

        // 오브젝트에 데이터를 로드한다.
        SaveDataManager.instance.LoadToObject();
    }

    public void ChangeSceneToMain()
    {
        // 메타데이터를 초기화.
        SaveDataManager.instance.LoadedMetaData = new SaveData
        {
            index = -1,
            player_pos_x = 0.0f,
            player_pos_y = 0.0f,
            camera_limit = false,
            happy_gauge = 0,
            depress_gauge = 0,

            memory_history_list = new List<bool>() { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
            afed_history_list = new List<bool>() { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },

            sPrologue = false,
            sNPC1 = false,
            sNPC2 = false,
            sNPC3 = false,
            sCave = false,
            sExit = false,

            start_time = 0.0f,
            end_time = 0.0f,
            start_date = ""
        };

        // 메인씬으로 전환한다.
        StartCoroutine(FadeInAndLoadMainScene());
    }

    public IEnumerator FadeInAndLoadMainScene()
    {
        float fadeCount = 0;
        
        while (fadeCount < 1.0f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            blackPanel.color = new Color(0, 0, 0, fadeCount); // 해당 변수 값으로 알파 값 지정
        }
        
        SceneManager.LoadScene("MainScene");
    }
}
