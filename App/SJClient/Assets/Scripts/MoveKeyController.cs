using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveKeyController : MonoBehaviour
{
    private GameObject player;
    private PlayerController playerController;
    private MemoryDataManager memoryDataManager;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
        memoryDataManager = GameObject.Find("Managers").GetComponent<MemoryDataManager>();
    }

    public void LeftDown()
    {
        playerController.inputLeft = true;
    }

    public void LeftUp()
    {
        playerController.inputLeft = false;
    }

    public void RightDown()
    {
        playerController.inputRight = true;
    }

    public void RightUp()
    {
        playerController.inputRight = false;
    }

    public void UpDown()
    {
        playerController.inputUp = true;
    }

    public void UpUp()
    {
        playerController.inputUp = false;
    }

    public void DownDown()
    {
        playerController.inputDown = true;
    }

    public void DownUp()
    {
        playerController.inputDown = false;
    }
    public void ResearchClick()
    {
        StartCoroutine(playerController.OnTriggerReSeacrch());
    }
    public void InventoryClick()
    {
        // 기억 인벤토리를 연다. 
        memoryDataManager.memoryPanel.SetActive(true);

        // 오브젝트에 데이터를 로드한다.
        memoryDataManager.LoadToObject();
    }
    public void InventoryClose()
    {
        // 기억 인벤토리를 닫는다.
        memoryDataManager.memoryPanel.SetActive(false);
    }
}
