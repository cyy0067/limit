using System.Collections.Generic;

// ----- 주인공 방 ----- //

public static class MCR_INFO
{
    // 침대
    public static List<KeyValuePair<string, string>> MCR_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잠을 자는 공간이다."),
    };

    // 스탠드
    public static List<KeyValuePair<string, string>> MCR_STAND = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "그냥 밝은 스탠드이다."),
    };

    // 행거
    public static List<KeyValuePair<string, string>> MCR_HANGER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "옷을 걸 수 있는 행거다."),
    };

    // 화분
    public static List<KeyValuePair<string, string>> MCR_FLOWERPOT = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "시들어버린 화분이다."),
        new KeyValuePair<string, string>("", "다시 살릴 수는 없을 것 같다."),
    };

    // 서랍장
    public static List<KeyValuePair<string, string>> MCR_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "낡은 서랍장이다."),
        new KeyValuePair<string, string>("", "서랍이 가볍다."),
    };

    // 사진
    public static List<KeyValuePair<string, string>> MCR_PHOTO = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "알아볼 수 없는 사진이다."),
    };

    // 인형
    public static List<KeyValuePair<string, string>> MCR_DOLL = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "처음 보는 곰인형이다."),
    };

    // 창문
    public static List<KeyValuePair<string, string>> MCR_WINDOW = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "굳게 닫혀있다."),
    };

    // 의자(위)
    public static List<KeyValuePair<string, string>> MCR_CHAIR1 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "먼지가 쌓인 의자이다."),
    };

    // 의자(아래)
    public static List<KeyValuePair<string, string>> MCR_CHAIR2 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "왜 두 개나 있는 걸까"),
    };
}


// ----- 거실 ----- //

public static class LR_INFO
{
    // 싱크대
    public static List<KeyValuePair<string, string>> LR_SINK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "물 한 방울 없다."),
    };

    // 식칼/도마
    public static List<KeyValuePair<string, string>> LR_CUTTING_BOARD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "제법 사용한 듯 무뎌 보인다."),
    };

    // 화로
    public static List<KeyValuePair<string, string>> LR_BRAZIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "사용한지 오래되어 보인다."),
    };

    // 빵
    public static List<KeyValuePair<string, string>> LR_BREAD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "상태가 별로 좋지 못한 빵이다."),
    };

    // 부엌 의자
    public static List<KeyValuePair<string, string>> LR_CK_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "식탁 앞에 앉아 밥을 먹기 위한 의자이다."),
        new KeyValuePair<string, string>("", "개수만큼 식구가 있었던 걸까."),
    };

    // 화분
    public static List<KeyValuePair<string, string>> LR_FLOWERPOT = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "관리를 하지 않아 시들었다."),
    };

    // 책
    public static List<KeyValuePair<string, string>> LR_BOOK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "무슨 내용인지 알 수 없다."),
    };

    // 시계
    public static List<KeyValuePair<string, string>> LR_WATCH = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "시곗바늘이 움직이지 않는다."),
    };

    // 벽난로
    public static List<KeyValuePair<string, string>> LR_FIRE_PLACE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "장작만 덩그러니 있다."),
    };

    // 거실 의자
    public static List<KeyValuePair<string, string>> LR_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "의자가 두 개뿐이다."),
    };

    // 책장
    public static List<KeyValuePair<string, string>> LR_BOOKSHELF = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "알 수 없는 책들이 많다."),
    };
}


// ----- 부모 방 ----- //

public static class PR_INFO
{
    // 침대
    public static List<KeyValuePair<string, string>> PR_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "방금 전에 봤던 침대보다 더 크다."),
    };

    // 서랍
    public static List<KeyValuePair<string, string>> PR_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "평범한 서랍이다."),
        new KeyValuePair<string, string>("", "방금 전에 봤던 것과 달리 제법 무겁다."),
    };

    // 스탠드
    public static List<KeyValuePair<string, string>> PR_STAND = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "방금 전에 봤던 것과 같은 것이다."),
    };

    // 거울
    public static List<KeyValuePair<string, string>> PR_MIRROR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "평범한 거울이다."),
        new KeyValuePair<string, string>("", "얼굴이 낯설다."),
    };

    // 행거
    public static List<KeyValuePair<string, string>> PR_HANGER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "방금 전에 봤던 것과 같은 것이다."),
    };

    // 테이블
    public static List<KeyValuePair<string, string>> PR_TABLE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "알 수 없는 종이가 많다."),
        new KeyValuePair<string, string>("", "뭐라고 써져 있는 걸까."),
    };

    // 의자
    public static List<KeyValuePair<string, string>> PR_CHAIR = new List<KeyValuePair<string, string>>()
{   
        new KeyValuePair<string, string>("", "의자가 하나뿐이다."),
    };

    // 창문
    public static List<KeyValuePair<string, string>> PR_WINDOW = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "뻑뻑해 힘으로 열기 힘들다."),
    };
}

// ----- 마을 ----- //

public static class VG_INFO
{
    // 주인공 우편함
    public static List<KeyValuePair<string, string>> VG_MC_MAILBOX = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "안에 아무것도 없다."),
        new KeyValuePair<string, string>("", "자주 우편함을 열었던 기억이 난다."),
        new KeyValuePair<string, string>("", "오늘은 무엇이 들어있을까 기대하며 열었던 마음이 떠올려진다."),
    };

    // 주인공 우편함 기억 값
    public static List<KeyValuePair<string, string>> VG_MC_MAILBOX_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "0"),
        new KeyValuePair<string, string>("emotion", "호기심"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "1"),
        new KeyValuePair<string, string>("title", "항상 궁금한 우편함"),
        new KeyValuePair<string, string>("contents", "닫혀 있는 우편함을 보면 안에 무엇이 들어있을까 매번 생각했다. 있을까, 없을까. 두근거리는 가슴에 손을 얹고 오늘도 어김없이 우편함을 열었다. 안에 아무것도 없었다. 다음에는 들어있을까."),
    };

    // 주인공 항아리
    public static List<KeyValuePair<string, string>> VG_MC_JAR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "텅텅 비어있다."),
        new KeyValuePair<string, string>("", "계속 항아리 안을 쳐다보자, 익숙함이 느껴진다."),
        new KeyValuePair<string, string>("", "지금처럼 항아리 안을 유심히 쳐다보다 미끄러져 안으로 들어간 적이 있음을 떠올린다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“어둡고… 무서웠었는데.”"),
    };

    // 주인공 항아리 기억 값
    public static List<KeyValuePair<string, string>> VG_MC_JAR_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "1"),
        new KeyValuePair<string, string>("emotion", "무서움"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "무서운 항아리"),
        new KeyValuePair<string, string>("contents", "오늘 나는 항아리가 무서운 것이라는 걸 알았다. 깊은 어둠 안으로 나를 집어삼키려고 했다. 아무것도 보이지 않아 덜컥 눈물이 났다. 친했던 아저씨가 나를 구해준 덕분에 빠져나올 수 있었다. 앞으론 항아리를 조심해야 할 것 같다."),
    };

    // 주인공 빨랫줄
    public static List<KeyValuePair<string, string>> VG_MC_CLOTHESLINE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "축축한 이불이 줄에 걸려 있던 게 머릿속에서 서서히 선명해진다."),
        new KeyValuePair<string, string>("", "이불에 지도를 그렸던 기억이다. 입안이 마르고 손끝이 차가워지는 감각이 느껴진다."),
        new KeyValuePair<string, string>("", "'어떤 벌을 받게 될까….' 과거 자신이 했던 말이 귓가에 맴돈다."),
    };

    // 주인공 항아리 기억 값
    public static List<KeyValuePair<string, string>> VG_MC_CLOTHESLINE_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "2"),
        new KeyValuePair<string, string>("emotion", "불안함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "실수를 했다."),
        new KeyValuePair<string, string>("contents", "오늘 이불에 지도를 그려버렸다. 부모님은 책임을 스스로 지라고 하셨다. 그래서 빨래를 직접 했다. 밖으로 나가 빨랫줄에 거는 건 엄마가 해주셨다. 어떤 벌을 받게 될까. 나는 두려웠다. 집에 들어가기 무서웠다. 내가 잘못한 거지만 벌은 언제나 두렵고 무서웠다. 하지만 들어가야 했다. 안 들어가면 더 무서운 벌이 다가올 테니까. "),
    };

    // 주인공 바구니
    public static List<KeyValuePair<string, string>> VG_MC_BASKET = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아무런 냄새가 나지 않는다. 먼지로 가득 쌓여져 있다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“옛날에는……”"),
        new KeyValuePair<string, string>("", "빨래를 담았던 바구니가 비워지면 산뜻한 향기가 코를 찔렀다. 그 상쾌한 향기 맡는 것을 좋아했었던 것 같다."),
    };

    // 주인공 바구니 기억 값
    public static List<KeyValuePair<string, string>> VG_MC_BASKET_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "3"),
        new KeyValuePair<string, string>("emotion", "상쾌함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "2"),
        new KeyValuePair<string, string>("title", "빨래하고 난 뒤 바구니는 항상 좋다."),
        new KeyValuePair<string, string>("contents", "젖은 빨래가 빨랫줄에 걸려 있다. 덩그러니 남은 바구니에 나는 항상 가까이 다가가 코를 박는다. 오늘도 그랬다. 산뜻한 향기가 코를 간지럽혔다. 항상 껴안고 자고 싶을 만큼 나는 이 바구니 냄새를 좋아한다. 냄새를 맡으면 머릿속이 상쾌해지는 기분이다. "),
    };

    // 주인공 밭
    public static List<KeyValuePair<string, string>> VG_MC_FIELD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "세 사람이 함께 밭을 가꾸던 모습이 눈앞에서 아른거린다. 모두 웃는 얼굴이다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“유일한 시간….이었어.”"),
        new KeyValuePair<string, string>("", "오직 밖에서만 허용되는 유일한 시간이었다."),
        new KeyValuePair<string, string>("", "그걸 인지하고 있음에도 아이는 그 시간을 좋아했다."),
        new KeyValuePair<string, string>("", "모든 걸 끝내고 나면 뿌듯한 감정이 올라가 땀 범벅이 되어도 개운한 느낌이 든 기억이 떠오른다."),
    };

    // 주인공 밭 기억 값
    public static List<KeyValuePair<string, string>> VG_MC_FIELD_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "4"),
        new KeyValuePair<string, string>("emotion", "뿌듯함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "정말 행복한 날이었다."),
        new KeyValuePair<string, string>("contents", "부모님과 함께 밖에서의 시간을 보냈다. 집 앞 밭을 가꿨다. 밖에서의 부모님은 항상 웃는 얼굴이셔서 좋다. 오직 밖에서만 이뤄지는 유일한 시간은 나를 항상 행복하게 만들었다. 기분이 좋아서 그런지 힘든 줄도 모르고 밭을 열심히 가꿨다. 땀 범벅이 되었지만 가꿔진 밭을 보니 뿌듯했다."),
    };

    // 판매상인 테이블
    public static List<KeyValuePair<string, string>> VG_VENDOR_TABLE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "처음 보는 물건과 반짝이는 액세서리 놓여 있다."),
        new KeyValuePair<string, string>("", "예전에도 이 광경을 자주 봐왔다. 지금과는 사뭇 다른 종류였지만 두근거림은 여전한 거 같다."),
        new KeyValuePair<string, string>("", "그때도 아이는 흥분을 가라앉히지 못했다. 신기한 물건을 볼 때면 그랬다. 외출이 허용되는 시간이 찾아오면 매번 들릴 정도로 이곳을 좋아했다."),
    };

    // 판매상인 테이블 기억 값
    public static List<KeyValuePair<string, string>> VG_VENDOR_TABLE_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "5"),
        new KeyValuePair<string, string>("emotion", "흥분"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "2"),
        new KeyValuePair<string, string>("title", "오늘은 신기한 걸 잔뜩 봤다."),
        new KeyValuePair<string, string>("contents", "외출이 허용된 날에 나는 아저씨의 말을 듣고 집 밑으로 내려가 봤다. 어떤 아저씨가 테이블에 이것저것 올려놓고 무언가를 팔고 있었다. 처음 보는 물건에 나는 눈을 뗄 수 없었다. 반짝반짝하고 신기하게 생긴 것이 많았다. 보고만 있어도 좋았다. 계속 봐도 질리지 않았다."),
    };

    // 판매상인 모자
    public static List<KeyValuePair<string, string>> VG_VENDOR_HAT = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "모자가 걸려있다. "),
        new KeyValuePair<string, string>("", "핑크색에 리본이 달려 그다지 흥미가 가지 않는다."),
        new KeyValuePair<string, string>("", "이전에도 이런 적이 있었음을 아이는 생각한다. 그때도 마찬가지로 시시한 모자로 인해 실망감을 모자가 걸려있다. 기대가 큰 만큼 실망감이 컸다."),
    };

    // 판매상인 모자 기억 값
    public static List<KeyValuePair<string, string>> VG_VENDOR_HAT_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "6"),
        new KeyValuePair<string, string>("emotion", "실망"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "실망한 날이었다."),
        new KeyValuePair<string, string>("contents", "오늘도 어김없이 상인 아저씨가 있는 곳으로 갔다. 당연히 부모님이 허락한 외출 시간에 나갔다. 짧은 시간이었지만 그래도 좋았다. 얼른 달려가 테이블 옆에 놓인 모자부터 봤다. 그리고 나는 실망했다. 테이블에 놓인 것처럼 신기한 물건들이 있을 줄 알았는데 아니었다. 평범한 모자였다. 상인 아저씨라면 내 마음을 흔들 것들만 준비할 줄 알았는데 아니었나 보다."),
    };

    // 판매상인 바구니
    public static List<KeyValuePair<string, string>> VG_VENDOR_BASKET = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "맛있어 보이는 사과와 알 수 없는 풀이 가득 담겨 있다. 판매하는 상품 같다."),
        new KeyValuePair<string, string>("", "먹음직스러운 사과로 인해 침이 저절로 삼켜진다. 아이는 자기 자신도 모르게 손이 앞으로 뻗어져 나간다."),
        new KeyValuePair<string, string>("", "그러자 옆에 있던 상인이 소리를 버럭 지르며 저지시킨다. 아이는 움찔하며 손을 멈춘다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“이번엔 안 때리시는 걸…까.”"),
        new KeyValuePair<string, string>("", "아이는 그 상인에게 손등을 맞은 기억을 떠올린다. 아프긴 해도 다른 고통에 비하면 그리 크지 않다고 생각했다."),
        new KeyValuePair<string, string>("", "아이는 맞지도 않은 손등을 문지르며 쳐다본다."),
    };

    // 판매상인 바구니 기억 값
    public static List<KeyValuePair<string, string>> VG_VENDOR_BASKET_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "7"),
        new KeyValuePair<string, string>("emotion", "놀람"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "상인 아저씨한테 손등을 맞았다."),
        new KeyValuePair<string, string>("contents", "먹음직스러워 보이는 사과가 바구니에 담겨 있었다. 침이 꿀꺽 삼켜졌다. 나도 모르게 사과를 쳐다보다 손을 내밀고 말았다. 상인 아저씨는 내가 훔쳐 가는 줄 알았나 보다. 내 손등을 세게 치셨다. 빨개진 손등을 다른 손으로 감싸 비벼댔다. 아프긴 했지만, 다른 아픔에 비하면 그렇게 큰 것도 아니었다."),
    };

    // 판매상인 박스
    public static List<KeyValuePair<string, string>> VG_VENDOR_BOX = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 가만히 박스를 쳐다본다. 왠지 모를 울적함이 밀려온다."),
        new KeyValuePair<string, string>("", "아이는 손을 내밀어 박스를 만져본다. 몸을 쭈그려앉아 박스에 등을 기댄 기억이 서서히 떠오르기 시작한다."),
        new KeyValuePair<string, string>("", "마음이 어수선할 때마다 이곳을 찾아와 몸을 숨기듯 웅크렸다. 그럴 때면 상인은 아이를 건드리지 않고 가만히 뒀다. 아이는 혼자 이곳에 있으면서 마음을 정리하고 가다듬었다."),
    };

    // 판매상인 박스 기억 값
    public static List<KeyValuePair<string, string>> VG_VENDOR_BOX_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "8"),
        new KeyValuePair<string, string>("emotion", "울적함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "상인 아저씨는 착하신 분인 걸까."),
        new KeyValuePair<string, string>("contents", "부모님께 혼났다. 분명 나를 사랑하는데 내가 잘못해서 그런 거겠지. 그래도 울적한 마음은 사라지지 않았다. 신기한 걸 보면 마음이 괜찮아지지 않을까 해서 집 밑으로 내려갔다. 하지만 눈에 들어오지 않았다. 우울한 마음을 어떻게 떼어낼 수 없었다. 혼자 있고 싶었다. 가만히 있고 싶었다. 그래서 테이블 옆에 놓인 박스로 갔다. 사이사이 좁은 틈으로 들어가자 조금 안정이 되었다. 다리를 웅크리고 머리를 숙였다. 상인 아저씨는 내가 슬픈 걸 아는 것 같았다. 나를 가만히 내버려 두었다. 평소 같았으면 쫓아냈을 텐데. 아저씨는 사실 착하신 분인 걸까."),
    };

    // 판매상인 천
    public static List<KeyValuePair<string, string>> VG_VENDOR_CLOTH = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "판매하는 상품이 천 위로 놓여 있다."),
        new KeyValuePair<string, string>("", "아이는 상인과 상품들을 번갈아가며 쳐다본다."),
        new KeyValuePair<string, string>("", "한때 들었던 말이 귓가에 맴돈다. 상인이 내뱉은 말은 아이를 기쁘고 즐겁고, 만족을 느끼게 만들었다."),
        new KeyValuePair<string, string>("", "남들이 이상적인 가족의 모습, 가족애가 넘친다고 말할 때마다 아이는 부모가 저를 많이 사랑한다고 생각했다."),
    };

    // 판매상인 천 기억 값
    public static List<KeyValuePair<string, string>> VG_VENDOR_CLOTH_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "9"),
        new KeyValuePair<string, string>("emotion", "행복"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "기쁜 말을 들었다."),
        new KeyValuePair<string, string>("contents", "오늘은 부모님과 함께 밑으로 내려갔다. 테이블이 아니라 바닥에 놓인 물건들을 봤다. 내가 쭈그려 앉아 쳐다보고 이것저것 가리키며 부모님과 말하고 있을 때 상인 아저씨가 와서 말했다. 상인 아저씨 눈에는 우리 가족이 사랑이 넘쳐 보이는 것 같다. 부모님이 나를 향한 사랑이 확실히 보인다고 했다. 나는 그 말을 듣고 기분이 좋았다. 역시 부모님은 나를 사랑하고 있는 거야. 다른 사람들이 그렇게 보이는 거면 확실한 거야. 부모님은 나를 많이 사랑해."),
    };

    // 집1 통
    public static List<KeyValuePair<string, string>> VG_HOME1_BARREL = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 리펠드 ]", "“이 통….”"),
        new KeyValuePair<string, string>("", "반사적으로 아이는 침을 꿀꺽 삼켰다."),
        new KeyValuePair<string, string>("", "이 집 근처를 지날 때 아저씨와 눈이 마주치면 아이를 부르던 손짓이 떠오른다. 그리고 통 안에 담긴 것을 줬었다."),
        new KeyValuePair<string, string>("", "지금 침이 저절로 삼켜질 정도로 아이는 그게 맛있었다는 걸 기억해 낸다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“아무런 이유 없이 그냥 주셨었는데.”"),
    };

    // 집1 통 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME1_BARREL_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "10"),
        new KeyValuePair<string, string>("emotion", "고마움"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "보물 통이 아닐까."),
        new KeyValuePair<string, string>("contents", "아저씨네 집 근처를 지나가던 중에 아저씨와 눈이 마주쳤다. 가까이 오라는 듯이 손을 흔들기에 나는 아저씨가 있는 곳으로 달려갔다. 그러자 아저씨는 통 안에서 무언갈 꺼내시더니 나한테 먹여줬다. 달콤한 맛에 미소가 지어졌다. 너무 맛있었다. 맛있는 걸 준 아저씨가 너무 좋고 고마웠다. 집으로 돌아가는 길 그 맛이 계속 생각나 입안에 침이 고였다."),
    };

    // 집1 밭
    public static List<KeyValuePair<string, string>> VG_HOME1_FIELD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잘 자라고 있는 밭을 아이는 멍하니 바라본다. 그리고 이내 주춤하며 한두 발자국 뒤로 물러난다."),
        new KeyValuePair<string, string>("", "그때처럼 벌레가 있을까 잎에 시선이 집중된다."),
        new KeyValuePair<string, string>("", "아저씨를 돕기 위해 밭에 왔었을 때 아이는 벌레와 직면했다."),
        new KeyValuePair<string, string>("", "몸이 굳고 눈동자가 이리저리 흔들렸다. 주저함에 이러지도 저러지도 못했던 기억이 떠오른다."),
    };

    // 집1 밭 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME1_FIELD_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "11"),
        new KeyValuePair<string, string>("emotion", "주저함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "벌레는 무서워."),
        new KeyValuePair<string, string>("contents", "아저씨의 부탁으로 함께 밭을 가꾸게 되었다. 열심히 손을 움직이던 중에 나를 멈추게 만든 것이 있었다. 그건 바로 벌레였다. 벌레가 잎을 열심히 먹고 있었다. 몸이 굳어 손이 움직이지 않았다. 어쩌면 좋을지 계속 고민했다. 하지만 그렇다고 아저씨한테 말할 수도, 그 자리에서 주저앉아 덜덜 떨 수도 없었다. 그럼 아저씨한테 폐를 끼칠 테니까. 그래서 혼자 힘으로 해결하려고 했지만 주저할 수밖에 없었다. 난 벌레가 무서우니까."),
    };

    // 집2 바구니 빨랫줄
    public static List<KeyValuePair<string, string>> VG_HOME2_BASKET_CLOTHESLINE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "방금 빨래를 널어놓은 것 같다."),
        new KeyValuePair<string, string>("", "수분기가 남은 바구니와 빨랫줄에 걸린 축축한 이불이 이를 증명하고 있다."),
    };

    // 집3 바구니 빨랫줄
    public static List<KeyValuePair<string, string>> VG_HOME3_BASKET_CLOTHESLINE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이의 집처럼 아무것도 걸려있지 않다."),
        new KeyValuePair<string, string>("", "하지만 먼지가 쌓여 있는 건 아니었다."),
    };

    // 집3 바구니 빨랫줄
    public static List<KeyValuePair<string, string>> VG_HOME3_JAR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "집안에 있는 것과 같은 항아리다. 안에 아무것도 없는 빈 항아리도 보인다."),
        new KeyValuePair<string, string>("", "그래도 역시 아이의 힘으로는 빈 항아리도 움직이기 힘들다."),
    };

    // 집4 울타리
    public static List<KeyValuePair<string, string>> VG_HOME4_FENCE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "집안에 있는 것과 같은 항아리다. 안에 아무것도 없는 빈 항아리도 보인다."),
        new KeyValuePair<string, string>("", "그래도 역시 아이의 힘으로는 빈 항아리도 움직이기 힘들다."),
    };

    // 집4 울타리 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME4_FENCE_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "20"),
        new KeyValuePair<string, string>("emotion", "서러움"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "오늘은 억울하고 서러운 날이었다."),
        new KeyValuePair<string, string>("contents", "오늘은 다리를 건너 반대편으로 가려고 했다. 그런데 가는 길에 밭이 하나 보여 그곳으로 몸을 돌렸다. 막 새싹이 자라고 있었다. 무엇으로 자랄까 궁금해 예상을 해보고 있었다. 그런데 갑자기 집주인 아저씨가 나타나 나에게 큰소리를 치셨다. 밭 안으로 들어가 새싹을 망가트리려고 하는 줄 아셨나 보다. 난 그냥 보려고만 했는데. 야단치시는 소리에 놀라 아직도 가슴이 쿵쾅쿵쾅 뛴다. 속상하고 서러운 날이었다."),
    };

    // 집5 우물
    public static List<KeyValuePair<string, string>> VG_HOME5_WELL = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 고개를 내밀어 밑을 내려다본다."),
        new KeyValuePair<string, string>("", "깊은 어둠 끝으로 물 냄새가 난다. 약간 찰랑거리는 물소리도 들려온다."),
    };

    // 집5 포대
    public static List<KeyValuePair<string, string>> VG_HOME5_BAG = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "무엇이 들어있는지 알 수 없는 포대가 두 자루 있다. 단단히 묶여 열 수 없다."),
        new KeyValuePair<string, string>("", "아이의 힘으로는 움직이는 것도 불가능하다. "),
    };

    // 집5 우편함
    public static List<KeyValuePair<string, string>> VG_HOME5_MAILBOX = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 우편함 안을 유심히 들여다본다. 어두워 뭐가 제대로 보이진 않는다."),
        new KeyValuePair<string, string>("", "비록 지금은 가만히 서 있지만, 예전엔 이 우편함 안으로 손을 넣었던 기억이 난다."),
        new KeyValuePair<string, string>("", "이곳 작은 병원에 한 분 계시는 의사선생님의 심부름이었다. 우편함으로 가 우편물을 꺼내오는 것이었다."),
        new KeyValuePair<string, string>("", "쉬운 일이었기에 금방 끝낼 수 있었다."),
        new KeyValuePair<string, string>("", "아이는 심부름을 하는 동안 마음이 들떴었다. 마치 이 병원의 한 직원이 된 기분을 느꼈었다."),
    };

    // 집5 우편함 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME5_MAILBOX_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "21"),
        new KeyValuePair<string, string>("emotion", "신남"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "병원에서 일한 기분이었다."),
        new KeyValuePair<string, string>("contents", "오늘은 의사 선생님이 바쁘셔서 심부름을 하게 되었다. 우편함에서 가서 우편물을 가져오는 일이었다. 간단한 일이라 금방 끝낼 수 있었지만 그 잠깐의 시간 동안 난 들떴다. 병원의 직원이 되어 일한 기분이었다. 나도 나중에 커서 이곳에서 일할 수 있을까. 만약 일하면 부모님이 좋아해 주실까. 나는 미래를 상상해 봤다."),
    };

    // 집5 울타리
    public static List<KeyValuePair<string, string>> VG_HOME5_FENCE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잘 관리되어 자라고 있는 밭이 눈에 들어온다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“의사선생님이 가꾸신…거였지.”"),
        new KeyValuePair<string, string>("", "아이는 작은 감탄과 함께 예전에도 이런 감정을 느낀 적 있음을 떠올린다."),
        new KeyValuePair<string, string>("", "병원에서 의사로 일을 하고, 취미생활로 이렇게 밭을 가꾸는 모습을 본 아이는 눈을 반짝거렸었다."),
        new KeyValuePair<string, string>("", "그의 모습이 아이의 눈에는 마냥 대단해 보였다. 자신도 커서 그와 같은 사람이 되고픈 마음을 먹기까지 했었다."),
    };

    // 집5 울타리 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME5_FENCE_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "22"),
        new KeyValuePair<string, string>("emotion", "존경심"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "역시 의사 선생님은 멋진 어른이다."),
        new KeyValuePair<string, string>("contents", "의사 선생님은 대단하시다. 병원 일도 하시고, 취미로 밭도 가꾸신다. 일할 때 모습이 엄청 멋있으시고, 밭을 관리하시는 모습도 멋지시다. 거기다 잘 가꾸시기까지 하신다. 나는 의사 선생님이 존경스럽다. 나중에 커서 의사 선생님 같은 어른이 되고 싶다."),
    };

    // 집6 빈 나무
    public static List<KeyValuePair<string, string>> VG_HOME6_BLANK_TREE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "나무 표면이 거칠지 않아 앉아도 아무 문제 없을 거 같다."),
    };

    // 집6 젖은 장작
    public static List<KeyValuePair<string, string>> VG_HOME6_WET_FIREWOOD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "장작이 물에 흥건하게 젖어있다. 불을 끈지 얼마 안 되어 보인다."),
        new KeyValuePair<string, string>("", "아이는 젖은 장작임을 확인하고 숨을 깊게 내뱉는다. 손에 힘이 들어간다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“내가 잘못해서.. 내가 혼날 짓을 해서.. 그래서 부모님이 날 교육하시려고 혼내신 거야. 응… 분명 그럴 거야.”"),
        new KeyValuePair<string, string>("", "아이는 부모님이 언성을 높임과 동시에 입을 움직이는 모습과 함께 얼굴에 가까워지는 손바닥을 머릿속으로 떠올린다."),
        new KeyValuePair<string, string>("", "눈을 질끔 감았다가 다시금 뜬다. 심호흡을 하고 아이는 다시 걸음을 옮긴다."),
    };

    // 집6 젖은 장작 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME6_WET_FIREWOOD_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "23"),
        new KeyValuePair<string, string>("emotion", "철렁함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "10"),
        new KeyValuePair<string, string>("title", "불은 위험해."),
        new KeyValuePair<string, string>("contents", "오늘 부모님께 혼났다. 이유를 알지 못했다. 그러니까 혼이 난 거겠지. 그런데 벌을 받던 도중 큰일이 날 뻔했다. 하마터면 벽난로에서 활활 타오르고 있는 불과 닿을 뻔했다. 다행히 뜨거운 것만 느끼고 상처가 나진 않았다. 가슴이 조마조마해 아직도 그때만 생각하면 몸이 떨린다. 심장도 많이 놀랐고, 벌 받아서 몸도 아프니까 오늘은 일찍 자야겠다."),
    };

    // 집6 장작
    public static List<KeyValuePair<string, string>> VG_HOME6_FIREWOOD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "장작이 쌓여져 있다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“옆에 있는 나무들이 잘린 거겠지.”"),
    };

    // 집6 도끼
    public static List<KeyValuePair<string, string>> VG_HOME6_AX = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "날카로운 도끼가 나무에 박혀져 있다."),
        new KeyValuePair<string, string>("", "아이는 도끼의 나무 막대 부분을 잡고 힘을 준다. 꿈쩍도 하지 않는다."),
        new KeyValuePair<string, string>("", "예전과 같았다. 혼자서는 무리라는 걸 다시금 깨닫는다."),
        new KeyValuePair<string, string>("", "아이는 아빠와 함께 패던 기억을 되새긴다. 따뜻했던 품이 등 뒤와 손등에서 느껴졌었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“그리고 옆구리가 욱신거리기도 했었지.”"),
    };

    // 집6 도끼 기억 값
    public static List<KeyValuePair<string, string>> VG_HOME6_AX_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "24"),
        new KeyValuePair<string, string>("emotion", "의지"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "장작 패기 잘 할 수 있었는데."),
        new KeyValuePair<string, string>("contents", "부모님과 함께 장작을 부탁드리러 갔다. 그런데 아저씨는 직접 패라고 시키셨다. 그래서 내가 나섰다. 어제 부모님을 실망시켜드렸으니 그걸 회복하고자 도끼를 잡았다. 하지만 멍이 든 허리가 욱신거리고 많이 무거워 들 수조차 없었다. 어쩔 수 없이 아빠가 도와줬다. 아빠의 품이 따뜻해 좋았지만 그래도 아쉬웠다. 허리가 아프지 않았으면 할 수 있었을까."),
    };

    // 동굴 앞 바위
    public static List<KeyValuePair<string, string>> VG_CAVE_ROCK = new List<KeyValuePair<string, string>>() { };
}

// ----- 집1 ----- //

public static class H1_INFO
{
    // 화분1
    public static List<KeyValuePair<string, string>> H1_FLOWER_POT1 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잘 가꾸어진 식물이다. 아이의 집에 있는 것과 다르게 푸릇푸릇하다."),
    };

    // 액자
    public static List<KeyValuePair<string, string>> H1_FRAME = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "액자에 시선이 간다. 아저씨와 그의 아내가 다정하게 있는 모습이 담겨 있다."),
        new KeyValuePair<string, string>("", "누가 봐도 행복해 보이는 부부의 모습이다."),
        new KeyValuePair<string, string>("", "지끈거리는 머리에 아이는 미간을 찌푸린다."),
        new KeyValuePair<string, string>("", "중요한 사진은 생각 나진 않지만, 분명 집에서 이런 액자가 있었음을 생각해낸다."),
        new KeyValuePair<string, string>("", "과거에 부부의 사진을 보고 행복함에 부러움을 느꼈었다. 하지만 그것도 잠시, 아이는 제 가족도 행복함이 가득했음을 되새긴 것이 곧이어 머릿속에 나타난다."),
    };

    // 액자 기억 값
    public static List<KeyValuePair<string, string>> H1_FRAME_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "12"),
        new KeyValuePair<string, string>("emotion", "부러움"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "우리 가족도 행복해."),
        new KeyValuePair<string, string>("contents", "오늘 아저씨네 집에 갔는데 벽에 액자가 걸려 있었다. 아저씨랑 아줌마였다. 다정해 보였다. 부모님이 아저씨랑 아줌마한테 다정하고 행복해 보이는 부부라고 말한 게 떠올랐다. 내가 봐도 아저씨랑 아줌마는 행복해 보였다. 그래도 역시 제일은 우리 가족이다. 우리 가족이 제일 행복해 보여. 부모님은 날 사랑하고, 나도 부모님을 사랑하니까."),
    };

    // 테이블, 싱크대
    public static List<KeyValuePair<string, string>> H1_TABLE_SINK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "물에 어느 정도 젖은 모습이 보인다."),
        new KeyValuePair<string, string>("", "사용한 지 얼마 안 된 듯하다."),
    };

    // 벽난로
    public static List<KeyValuePair<string, string>> H1_FIRE_PLACE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "벽난로에 불이 지펴져 있지 않다. 아직 불을 넣지 않은 건지 장작에 그을린 자국이 없다."),
        new KeyValuePair<string, string>("", "장작을 계속 쳐다보자 일렁거리는 불이 희미하게나마 눈앞에 나타난다. 직접 불 가까이 있는 것이 아님에도 불구하고 아이는 따뜻함이 느껴지는 것만 같았다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“나무가 타는 소리… 좋았는데.”"),
        new KeyValuePair<string, string>("", "아이는 평온함에 마음이 차분해지는 기분이었다."),
    };

    // 벽난로 기억 값
    public static List<KeyValuePair<string, string>> H1_FIRE_PLACE_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "13"),
        new KeyValuePair<string, string>("emotion", "평안함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "벽난로는 위험해."),
        new KeyValuePair<string, string>("contents", "부모님의 심부름으로 아저씨네에 갔다. 추워 몸을 덜덜 떨면서 갔다. 얼른 심부름을 끝내고 집에 가려고 했지만 아저씨가 몸을 녹이고 가라고 말씀해 주셨다. 난 아저씨 말대로 벽난로 앞에 앉았다. 그런데 따뜻한 것과 나무가 타는 소리, 움직이는 불을 봐서 그런지 잠이 쏟아졌다. 얼른 집에 가야 한다고 몸에 말해봤지만 듣질 않았다. 평안한 느낌 때문인지 어쩔 수가 없었다."),
    };

    // 테이블 의자
    public static List<KeyValuePair<string, string>> H1_TABLE_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "마주 보는 의자와 그 사이로 테이블이 놓여 있다."),
        new KeyValuePair<string, string>("", "아이는 테이블 위로 손을 얹어 문지른다."),
        new KeyValuePair<string, string>("", "고개를 천천히 움직여 한쪽에는 아저씨를, 다른 한쪽에는 아이 자신이 앉아 있던 모습을 떠올린다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“아저씨 얼굴…. 그린 적 있는 거 같아.”"),
        new KeyValuePair<string, string>("", "그림을 본 아저씨가 만족해 보여 뿌듯해하는 자신을 떠올린다."),
    };

    // 테이블 의자 기억 값
    public static List<KeyValuePair<string, string>> H1_TABLE_CHAIR_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "14"),
        new KeyValuePair<string, string>("emotion", "자신감"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "난 그림을 잘 그려."),
        new KeyValuePair<string, string>("contents", "아저씨가 자기 얼굴을 그려달라고 부탁하셨다. 난 당연히 된다고 대답했다. 그림에 자신이 있어 의자에 앉아 열심히 손을 움직였다. 집중해서 그런지 얼마나 시간이 흐른지도 몰랐다. 다 완성을 하고 아저씨께 보여드렸다. 아저씨는 기뻐하며 마구 칭찬해 주셨다. 칭찬에 쑥스러웠지만 기분은 좋았다. 역시 난 그림에 자신이 있나 보다."),
    };

    // 책장
    public static List<KeyValuePair<string, string>> H1_BOOKSHELF = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "다양한 책들이 책장을 가득 메우고 있다. 아이는 자신이 알아보지 못하는 책들에게서 고개를 돌린다."),
    };

    // 화분2
    public static List<KeyValuePair<string, string>> H1_FLOWER_POT2 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잘 자란 화분이 눈에 들어온다. 집에 있는 것과 같은 식물임을 아이는 알아본다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“잘 자란 건 이렇게 생겼구나.”"),
        new KeyValuePair<string, string>("", "가까이 다가가 식물을 자세히 살핀다."),
        new KeyValuePair<string, string>("", "귓가로 웃음소리와 대화 소리가 환청 마냥 들려오면서 잠들어 있던 기억이 깨어난다."),
        new KeyValuePair<string, string>("", "아이는 부모님과 함께 이 집으로 와 화분을 아저씨 부부에게 선물했다."),
        new KeyValuePair<string, string>("", "아이는 아직 덜 자란 식물을 보며 잘 자랄 수 있을지 궁금했었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“그게 지금 이렇게… 잘 키우셨구나.”"),
        new KeyValuePair<string, string>("", "그때 당시 잘 키우라고 신신당부하던 제 모습을 떠올린다."),
    };

    // 화분2 기억 값
    public static List<KeyValuePair<string, string>> H1_FLOWER_POT2_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "15"),
        new KeyValuePair<string, string>("emotion", "궁금함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "화분 잘 키우시겠지?"),
        new KeyValuePair<string, string>("contents", "부모님과 함께 아저씨 집으로 향했다. 집에 있는 것과 같은 화분을 아저씨께 선물로 드렸다. 아직 다 자라지 않아 아저씨가 잘 키우실까 궁금했다. 부모님과 아저씨, 아줌마가 얘기하고 있던 중에 나는 아저씨께 쑥쑥 잘 자라게 해달라고 부탁드렸다. 내 말에 모두가 웃었다. 왜 웃는지는 알 수 없었지만 즐거워 보였다. 아저씨는 자신만 믿으라며 말씀하셨고, 아줌마는 열심히 키우겠다고 말씀하셨다. 자주 놀러와 식물이 잘 자라는지 봐야겠다."),
    };

    // 침대
    public static List<KeyValuePair<string, string>> H1_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "제법 큰 침대가 놓여 있다. 아이의 방에 있는 침대보단 크고, 아이의 부모 방에 있던 침대와 비슷하다."),
    };

    // 서랍장
    public static List<KeyValuePair<string, string>> H1_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "서랍장이다."),
        new KeyValuePair<string, string>("", "제법 높아 위쪽에 있는 서랍은 아이의 손에 닿지 않는다."),
    };
}

// ----- 집3 ----- //

public static class H3_INFO
{
    // 화분 1,2,3
    public static List<KeyValuePair<string, string>> H3_FLOWER_POTS = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잘 가꾸어진 식물이다. 아이의 집에 있는 것과 다르게 푸릇푸릇하다."),
    };

    // 화분 1,2,3 기억 값
    public static List<KeyValuePair<string, string>> H3_FLOWER_POTS_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "16"),
        new KeyValuePair<string, string>("emotion", "죄책감"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "마음이 무거운 날."),
        new KeyValuePair<string, string>("contents", "오늘은 실수를 했다. 아줌마가 아끼시는 화분을 깨트려버렸다. 실수였다. 너무 놀란 모습을 아줌마가 보셨는지 나에게 괜찮다고 말씀해 주셨다. 하지만 내 마음은 괜찮지 않았다. 나 때문에 화분이 깨지고 꽃이 망가졌다. 오늘 하루 종일 마음이 무거웠다."),
    };

    // 테이블 의자
    public static List<KeyValuePair<string, string>> H3_TABLE_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "테이블과 의자가 한 쌍뿐이다."),
    };

    // 냄비화로 테이블 항아리
    public static List<KeyValuePair<string, string>> H3_BRAZIER_TABLE_JAR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "요리를 한 지 얼마 안 된 것인지 맛있는 냄새가 났다."),
        new KeyValuePair<string, string>("", "아이는 냄새를 계속 맡다가 이내 어디선가 맡아본 적이 있음을 기억해 낸다."),
        new KeyValuePair<string, string>("", "집에서 만든 음식을 이 집의 주인인 아줌마께 전달했을 때의 일이었다. 아줌마는 고마움에 보답으로 아이를 기다리게 해 음식을 담기 시작했다."),
        new KeyValuePair<string, string>("", "아이는 빈손이 아닌 음식이 담긴 그릇을 두 손으로 들고 집으로 돌아갔었다."),
    };

    // 냄비화로 테이블 항아리 기억 값
    public static List<KeyValuePair<string, string>> H3_BRAZIER_TABLE_JAR_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "17"),
        new KeyValuePair<string, string>("emotion", "정겨움"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "엄마 요리가 더 맛있지 않을까."),
        new KeyValuePair<string, string>("contents", "엄마의 심부름을 하러 아줌마네 집으로 향했다. 엄마가 한 요리를 전해드리는 것이었다. 아줌마는 음식을 받고 엄청 좋아하셨다. 그리고 나에게 잠시 기다려 달라고 하곤 보답이라며 나에게 음식이 담긴 그릇을 주셨다. 맛있는 냄새가 났다. 왠지 모르게 마음이 따뜻해졌다. 나는 가벼운 발걸음으로 집에 갔다. 비록 아줌마가 주신 음식을 내가 먹어보진 못했지만 분명 엄마가 한 것이 더 맛있을 거라 나는 생각한다."),
    };

    // 항아리
    public static List<KeyValuePair<string, string>> H3_JAR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "안에 무엇이 들어 있는지 알 수 없다."),
        new KeyValuePair<string, string>("", "묵직한 무게 때문에 아이의 힘으로는 움직일 수 없다."),
    };

    // 작은 서랍
    public static List<KeyValuePair<string, string>> H3_SMALL_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "빛에 의해 손잡이가 반짝거린다. 아이는 조심스레 손잡이를 잡고 살살 당겨본다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“안 빠지네.”"),
        new KeyValuePair<string, string>("", "손잡이가 힘없이 툭 빠졌던 기억이 머릿속에서 스멀스멀 나타난다."),
        new KeyValuePair<string, string>("", "그때도 지금처럼 살살 당겼는데 힘없이 빠지는 손잡이에 당혹스러움을 느꼈던 기억이 난다. 얼떨떨함으로 인해 멍하니 손잡이를 바라봤었다."),
    };

    // 작은 서랍 기억 값
    public static List<KeyValuePair<string, string>> H3_SMALL_CHIFFONIER_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "18"),
        new KeyValuePair<string, string>("emotion", "얼떨떨함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "얼굴이 뜨거운 날이었다."),
        new KeyValuePair<string, string>("contents", "오늘은 당혹스러운 날이었다. 아줌마가 줄 것이 있다고 하셔서 잠시 집 안으로 들어갔는데, 기다리던 중에 아줌마가 서랍을 열어 물건을 꺼내달라고 부탁하셨다. 나는 부탁대로 작은 서랍 앞에 서서 손잡이를 잡고 당겼다. 그런데 서랍은 열리지 않고 손잡이가 툭 떨어졌다. 나는 놀라 멍하니 손잡이를 보기만 했다. 정신이 멍했다. 아무런 생각이 나지 않았다. 아줌마는 자주 이런다고 하셨지만 마음이 편하지 않았다. 부끄러워져 볼이 뜨거웠다."),
    };

    // 화장대 의자
    public static List<KeyValuePair<string, string>> H3_VANITY_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "거울에 아이의 모습이 비쳤다. 아이는 가만히 서서 거울 속 자신의 모습을 응시했다."),
        new KeyValuePair<string, string>("", "제 얼굴임에도 다소 낯섬이 느껴졌다."),
        new KeyValuePair<string, string>("", "거울을 계속 응시하자 그곳에서 엄마의 모습이 비쳐 보이기 시작했다."),
        new KeyValuePair<string, string>("", "화장을 한 아이의 엄마 얼굴이었다."),
        new KeyValuePair<string, string>("", "화장을 한 얼굴을 볼 때면 아이는 설레었다. 밖에 함께 외출할 것임을 짐작했기 때문이었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“물론 집안에서의 부모님도 좋았지만, 밖이… 더 좋았던 거 같아.”"),
    };

    // 화장대 의자 기억 값
    public static List<KeyValuePair<string, string>> H3_VANITY_CHAIR_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "19"),
        new KeyValuePair<string, string>("emotion", "설렘"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "엄마가 화장하면 설레어."),
        new KeyValuePair<string, string>("contents", "엄마가 화장을 하는 날이면 나는 설레어서 가슴이 두근거린다. 왜냐하면 함께 외출을 하는 것을 의미하기 때문이다. 부모님과 함께 밖을 나가는 건 항상 기쁘고 설렌다. 집 안에서의 부모님도 좋지만 밖에서의 부모님은 조금 더 친절하셔서 난 외출이 좋다. 밖에서의 부모님이 조금 더 나를 사랑하는 게 느껴진다. 물론 집에서도 나를 사랑해 주신다."),
    };

    // 침대
    public static List<KeyValuePair<string, string>> H3_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "혼자 자기에 알맞은 침대 사이즈다."),
        new KeyValuePair<string, string>("", "아이의 방에 있던 침대보다 더 폭신하다."),
    };

    // 서랍장
    public static List<KeyValuePair<string, string>> H3_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "화장대 옆에 있는 서랍장보다 크다. 손잡이가 잘 빠지지 않아 안심하고 열 수 있다."),
    };
}

// ----- 집5 ----- //

public static class H5_INFO
{
    // 사진
    public static List<KeyValuePair<string, string>> H5_PHOTO = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "사람의 신체 부위가 드러나고 어떠한 기관이 있는지 보여주는 사진이다."),
    };

    // 캐비넷
    public static List<KeyValuePair<string, string>> H5_CABINET = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "제법 큰 캐비닛이다. 아이는 손잡이를 잡고 당겨본다. 열기 다소 뻑뻑하다."),
        new KeyValuePair<string, string>("", "안에는 알 수 없는 것들로 가득하다. 의료 물품들로 보인다."),
        new KeyValuePair<string, string>("", "예전에도 이 캐비닛 안을 들여다본 적이 있음을 아이는 데자뷔마냥 떠올린다. "),
        new KeyValuePair<string, string>("", "의사인 그가 캐비닛 문을 열고 안에서 무언가를 꺼내고 있는 뒷모습을 바라봤었다."),
        new KeyValuePair<string, string>("", "그 모습이 아이의 눈에는 멋져 보였다."),
        new KeyValuePair<string, string>("", "아이는 커서 그와 같이 캐비닛을 열고 안에서 의료 물품을 꺼내는 상상을 했었다."),
        new KeyValuePair<string, string>("", "기대감에 가슴이 부풀었던 경험을 되새긴다."),
    };

    // 캐비넷 기억 값
    public static List<KeyValuePair<string, string>> H5_CABINET_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "28"),
        new KeyValuePair<string, string>("emotion", "기대감"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "나도 할 수 있겠지?"),
        new KeyValuePair<string, string>("contents", "의사 선생님이 일하시는 모습을 지켜봤다. 환자를 살피시더니 캐비닛으로 향해 문을 열고 안에서 무언가를 꺼냈다. 환자에게 가져가는 걸 보니 약인 거 같았다. 난 캐비닛을 여는 의사 선생님의 뒷모습이 너무나 멋져 보였다. 내가 커서 저렇게 캐비닛을 열고 물건을 꺼내는 모습을 상상하니 너무나 기대되었다. 얼른 나도 커서 의사 선생님이 되고 싶다."),
    };

    // 테이블 의자
    public static List<KeyValuePair<string, string>> H5_TABLE_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "진료를 보는 곳이다. 환자와 의자의 의자가 각각 놓여 있다."),
    };

    // 왼쪽 가림막
    public static List<KeyValuePair<string, string>> H5_L_CURTAIN = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "환자가 누운 침대를 가리기 위한 커튼이다."),
    };

    // 오른쪽 가림막
    public static List<KeyValuePair<string, string>> H5_R_CURTAIN = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "환자 침대를 가린 것과 같은 커튼이다. 개인 침대를 가리기 위한 용도인 거 같다."),
    };

    // 왼쪽 침대
    public static List<KeyValuePair<string, string>> H5_L_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "차가운 침대 프레임과 그렇게 푹신하진 않은 매트리스가 함께 놓여 있다."),
        new KeyValuePair<string, string>("", "아이는 침대를 살펴본다. 아팠을 적 여기 왔던 기억을 차차 되짚어본다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“감기 때문에 왔었는데.”"),
        new KeyValuePair<string, string>("", "의사의 말에 아이는 침대에 누워 긴장을 하고 있었다."),
        new KeyValuePair<string, string>("", "곧 주사가 다가올 것임에 눈을 질끔 감고 기다렸다. 하지만 다가온 건 주사가 아닌 질문이었다."),
        new KeyValuePair<string, string>("", "의사가 아이의 멍을 보곤 이에 대해 묻자, 아이는 얼굴이 빨개져선 입만 뻐금뻐금 움직였다."),
        new KeyValuePair<string, string>("", "아이는 창피함에 말을 제대로 하지 못했다. 자신이 벌받은 걸 말할 수는 없어 그저 입을 꾹 다물었던 기억이 난다."),
    };

    // 왼쪽 침대 기억 값
    public static List<KeyValuePair<string, string>> H5_L_BED_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "29"),
        new KeyValuePair<string, string>("emotion", "창피함"),
        new KeyValuePair<string, string>("isHappiness", "false"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "의사 선생님께 들키고 말았다."),
        new KeyValuePair<string, string>("contents", "오늘 너무나도 창피했다. 감기 때문에 병원을 혼자 가게 되었다. 의사 선생님이 주사를 놓아주신다고 하셔서 무서웠지만 얼른 나아야지 부모님이 걱정을 덜 하실 테니까 억지로 참고 누웠다. 그런데 주사를 맞기 전 의사 선생님이 내 몸을 보신 거 같다. 티셔츠를 위로 들어 올리시고 보신 거 같다. 내가 벌받은 걸 선생님이 보셨다. 너무 부끄럽고 창피했다. 의사 선생님께는 보여드리고 싶지 않았는데."),
    };

    // 오른쪽 침대
    public static List<KeyValuePair<string, string>> H5_R_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "환자 침대와는 다른 침대다. 아이는 슬쩍 엉덩이를 붙여본다. 푹신함에 눈이 크게 떠진다."),
        new KeyValuePair<string, string>("", "아이는 예전에도 이 침대에 앉아 의사인 그와 대화를 나눈 기억이 새록새록 난다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“집에 있는 내 침대보다 훨씬 푹신해.”"),
        new KeyValuePair<string, string>("", "아이는 위아래로 몸을 움직여 푹신함을 느끼곤 다시 일어선다."),
    };

    // 오른쪽 침대 기억 값
    public static List<KeyValuePair<string, string>> H5_R_BED_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "30"),
        new KeyValuePair<string, string>("emotion", "신기함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "침대가 엄청 푹신했다."),
        new KeyValuePair<string, string>("contents", "의사 선생님이 나랑 얘기를 하려고 침대에 앉으라고 하셨다. 그래서 난 침대 끝에 앉았다. 그런데 앉자마자 눈이 동그랗게 떠졌다. 엄청 푹신해 저도 모르게 감탄이 입 밖으로 나왔다. 집에 있는 내 침대보다 훨씬 푹신했다. 신기했다. 침대가 이렇게 푹신할 수 있구나."),
    };
}

// ----- 집6 ----- //

public static class H6_INFO
{
    // 침대
    public static List<KeyValuePair<string, string>> H6_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이의 방에 있던 침대와 크기가 비슷해 보인다."),
        new KeyValuePair<string, string>("", "아이는 제 방에서 떠올리지 못했던 걸 비슷한 침대의 모습에 조금씩 떠올리기 시작한다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“방에… 혼자 있을 때면 나가질 못해서 이불 안으로….”"),
        new KeyValuePair<string, string>("", "머리가 다소 지끈거린다."),
        new KeyValuePair<string, string>("", "아이는 어두운 이불 안으로 들어가 몸을 웅크리고 머리를 손으로 감쌌던 자신을 떠올린다."),
    };

    // 침대 기억 값
    public static List<KeyValuePair<string, string>> H6_BED_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "25"),
        new KeyValuePair<string, string>("emotion", "자책"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "다 내 잘못이야."),
        new KeyValuePair<string, string>("contents", "화가 나신 부모님은 나를 방에 넣으셨다. 내가 혼날 짓을 해서 마땅한 벌을 받는 것이겠지. 지금 내가 한 잘못을 생각해 보라는 뜻이겠지. 그래서 나는 이불 안으로 들어가 몸을 웅크리고 손으로 머리를 감쌌다. 다 나를 생각해서 하신 거고, 나를 사랑하니까 이렇게 벌도 주시는 것이라 생각했다."),
    };

    // 보관대
    public static List<KeyValuePair<string, string>> H6_STORAGE_STAND = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "밖에 있는 도끼를 이곳에 보관하는 듯하다."),
    };

    // 테이블 의자
    public static List<KeyValuePair<string, string>> H6_TABLE_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "한 명 앉을 수 있는 의자와 그에 알맞은 테이블이다."),
        new KeyValuePair<string, string>("", "아이는 제 집, 제 방에 있던 테이블과 의자를 떠올린다."),
        new KeyValuePair<string, string>("", "아이는 의자에 엉덩이를 붙이고 앉아 책을 읽었던 기억이 난다. 그때 당시 시간이 점점 흐를수록 마음은 불안했었다."),
        new KeyValuePair<string, string>("", "다 읽지 않으면 무슨 벌이 기다릴지 몰라 조마조마한 마음으로 읽기 시작하니 제대로 눈에 들어오지 않았었다."),
        new KeyValuePair<string, string>("", "그래도 꾸역꾸역 읽어보려 노력했다. 다 저를 위한 일이라 아이는 생각했었기에."),
    };

    // 테이블 의자 기억 값
    public static List<KeyValuePair<string, string>> H6_TABLE_CHAIR_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "26"),
        new KeyValuePair<string, string>("emotion", "초조함"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "책은 좋지만, 마음은 조마조마했어."),
        new KeyValuePair<string, string>("contents", "아빠는 나를 위해 내게 책을 읽으라고 오늘 시키셨다. 밖에서 부끄러움을 당하지 않기 위해서였다. 나를 생각해 주는 아빠를 위해서라도 책을 다 읽어보려 노력했다. 하지만 시간은 너무 미웠다. 어떨 때는 천천히 흐르던 시간이 오늘은 금방 지나고 말았다. 점점 약속한 시간이 다가와 마음이 초조했다. 책 읽는 건 좋았지만 시간이 방해를 했다. 아빠를 실망시키고 싶지 않았는데."),
    };

    // 부엌 테이블 싱크대
    public static List<KeyValuePair<string, string>> H6_TABLE_SINK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 부엌 테이블 위로 손을 올렸다. 차가운 온도가 손바닥에 닿는다. 아직 요리를 하지 않은 건지 메말라 있다."),
        new KeyValuePair<string, string>("", "아이는 자신이 집 부엌에서 요리를 했었던 기억을 되새겨본다."),
        new KeyValuePair<string, string>("", "부모님이 나가시고 혼자 점심을 해결해야 했기에 다소 여러 고난을 겪었지만 끝내 아이는 스스로 점심을 먹었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“칭찬은…받지 못했지만 부모님이 신경 쓰이도록 한 게 아니라서 다행이라 생각했었는데.”"),
    };

    // 부엌 테이블 싱크대 기억 값
    public static List<KeyValuePair<string, string>> H6_TABLE_SINK_MM = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "27"),
        new KeyValuePair<string, string>("emotion", "자랑스러움"),
        new KeyValuePair<string, string>("isHappiness", "true"),
        new KeyValuePair<string, string>("value", "5"),
        new KeyValuePair<string, string>("title", "오늘은 나 스스로 점심을 해먹은 날."),
        new KeyValuePair<string, string>("contents", "부모님이 밖으로 나가셨다. 그래서 혼자 점심을 먹어야 했다. 폐를 안 끼치고, 부모님을 신경 쓰이게 하고 싶진 않아 스스로 점심을 만들고자 했다. 조금 힘들고 난감한 상황이 있긴 했지만 그래도 끝내 잘 먹었다. 맛은 역시 엄마가 한 게 최고였다. 그래도 나 혼자 해낸 것이 자랑스러웠다. 비록 칭찬을 받진 못했지만 그래도 좋았다. 신경 쓰이도록 하지 않았으니까."),
    };

    // 포대 자루
    public static List<KeyValuePair<string, string>> H6_BAG = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "안에 무엇이 들어있는지 알 수 없다."),
        new KeyValuePair<string, string>("", "아이는 손을 내밀어 포대자루를 눌러봤다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“딱딱하지가 않네. 침대만큼은 아니지만 그래도 폭신해.”"),
    };

    // 행거
    public static List<KeyValuePair<string, string>> H6_HANGER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 익숙함에 발걸음을 멈춘다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“집에 있는 거랑 같아.”"),
        new KeyValuePair<string, string>("", "슬쩍 건드려보곤 금방 흥미를 잃는다."),
    };
}

// ----- NPC ----- //

public static class NPC_INFO
{
    // Village NPC1
    public static List<KeyValuePair<string, float>> VILLAGE_NPC1_DEFAULT_DIRECTION = new List<KeyValuePair<string, float>>()
    {
        new KeyValuePair<string, float>("MoveX", -1f),
        new KeyValuePair<string, float>("MoveY", 0f),
    };
    
    public static List<KeyValuePair<string, string>> VILLAGE_NPC1 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 아저씨 ]", "“부모님 일은 참 안됐어, 리펠드…. 참 좋은 분들이셨는데. 널 무척 아끼셨지.”"),
    };

    // Village NPC2
    public static List<KeyValuePair<string, float>> VILLAGE_NPC2_DEFAULT_DIRECTION = new List<KeyValuePair<string, float>>()
    {
        new KeyValuePair<string, float>("MoveX", 0f),
        new KeyValuePair<string, float>("MoveY", 0f),
    };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC2 = new List<KeyValuePair<string, string>>() { };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC2_B = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ ??? ]", "“리펠드구나. 기억을 잃었다면서? 난 이 위에 있는 언덕에 빨간 벽돌 집 있잖니? 거기 사는 아줌마야.”"),
        new KeyValuePair<string, string>("[ 빨간 벽돌집 아줌마 ]", "“부모님 일은 참 안되었어… 네가 쓰러져 있는 동안 내가 너희 부모님이 잠들어 계신 이곳을 관리하고 있었어.”"),
        new KeyValuePair<string, string>("[ 빨간 벽돌집 아줌마 ]", "“너를 많이 사랑해 주시던… 좋은 분들이셨는데.”"),
    };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC2_A = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 빨간 벽돌집 아줌마 ]", "“부모님 일은 참 안되었어… 네가 쓰러져 있는 동안 내가 너희 부모님이 잠들어 계신 이곳을 관리하고 있었어.”"),
        new KeyValuePair<string, string>("[ 빨간 벽돌집 아줌마 ]", "“너를 많이 사랑해 주시던… 좋은 분들이셨는데.”"),
    };

    // Village NPC3
    public static List<KeyValuePair<string, float>> VILLAGE_NPC3_DEFAULT_DIRECTION = new List<KeyValuePair<string, float>>()
    {
        new KeyValuePair<string, float>("MoveX", 1f),
        new KeyValuePair<string, float>("MoveY", 0f),
    };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC3 = new List<KeyValuePair<string, string>>() { };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC3_B = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ ??? ]", "“리펠드!! 정신을 차렸구나. 너 쓰러졌을 때 얼마나 가슴이 철렁했는지 아니. 깨어나서 정말 다행이야.”"),
        new KeyValuePair<string, string>("[ ??? ]", "“아 참 기억이 없다고 했지? 난 이 병원의 의사 선생님이야. 너랑 자주 놀았었지.”"),
        new KeyValuePair<string, string>("[ 의사 선생님 ]", "“기억이 돌아오면 알려줘, 리펠드. 선생님이 도울 게 있으면 말해주고. 그리고 예전 기억이 떠올라서… 상담할 게 있다면 꼭 말해줘.”"),
    };

    public static List<KeyValuePair<string, string>> VILLAGE_NPC3_A = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 의사 선생님 ]", "“기억이 돌아오면 알려줘, 리펠드. 선생님이 도울 게 있으면 말해주고. 그리고 예전 기억이 떠올라서… 상담할 게 있다면 꼭 말해줘.”"),
    };
}

// ----- 1차 엔딩 이후 주인공 방 ----- //

public static class AFED_MCR_INFO
{
    // 침대
    public static List<KeyValuePair<string, string>> AFED_MCR_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 아픈 몸을 이끌고 침대에 한껏 웅크린 채 누운 제 모습을 침대 위로 겹쳐본다."),
        new KeyValuePair<string, string>("", "정신을 잃은 동안 사라졌던 멍 자국이 있던 팔뚝을 지금에서야 문질러본다."),
        new KeyValuePair<string, string>("", "고통은 없지만 왠지 모르게 찌릿함이 느껴지는 기분을 아이는 느낀다."),
    };

    // 침대 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_BED_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "0"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 스탠드
    public static List<KeyValuePair<string, string>> AFED_MCR_STAND = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "한줄기의 유일한 빛 마냥 아이는 어둠 속에서 이 스탠드에서 뿜어져 나오는 빛을 보는 걸 좋아했다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“왠지 따뜻하게 느껴졌었는데.”"),
    };

    // 스탠드 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_STAND_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "1"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 행거
    public static List<KeyValuePair<string, string>> AFED_MCR_HANGER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "행거 안쪽, 벽 모서리로 들어가기도 했음을 떠올린다. 하지만 이 기억은 그다지 좋은 것은 아니었다."),
        new KeyValuePair<string, string>("", "아이는 심호흡을 한다."),
        new KeyValuePair<string, string>("", "다소 술 냄새가 나는 아빠가 머릿속에 나타난다. 그럴 때면 아이는 행거 구석으로 몸을 숨겼다."),
        new KeyValuePair<string, string>("", "눈에 띄게 되면 커다란 발이 날아왔다."),
        new KeyValuePair<string, string>("", "아이는 두 팔로 머리를 감싸느라 제대로 얼굴을 보진 않았으나 보고 싶지 않은 마음도 없지 않아 있었다."),
    };

    // 행거 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_HANGER_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "2"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 화분
    public static List<KeyValuePair<string, string>> AFED_MCR_FLOWERPOT = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 리펠드 ]", "“이건 첫 번째가 아니라 두 번째 화분이었어…”"),
        new KeyValuePair<string, string>("", "아이는 자신이 이 화분을 키웠던 것임을 떠올림과 동시에 이 전에 또 다른 식물을 이 화분에 키웠던 것을 떠올려낸다."),
        new KeyValuePair<string, string>("", "점점 다가오는 부모의 손이 두려웠던 아이는 뒷걸음질을 쳤었다. 그리고 뒤를 제대로 보지 못해 그만 화분을 엎어버리고 만 것이었다."),
    };

    // 화분 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_FLOWERPOT_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "3"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 서랍장
    public static List<KeyValuePair<string, string>> AFED_MCR_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "한두 어벌뿐인 옷이 안에 들어있다. 그것마저도 낡아 닳아있다."),
        new KeyValuePair<string, string>("", "아이는 새로운 옷을 부탁할 수 없어 그냥 입었다."),
        new KeyValuePair<string, string>("", "닳아진 옷을 들고 부모님 앞에 섰던 적이 있었다. 그때의 눈빛과 높아진 언성으로 인해 그날 이후론 꺼내지 않았다."),
    };

    // 서랍장 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_CHIFFONIER_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "4"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 사진
    public static List<KeyValuePair<string, string>> AFED_MCR_PHOTO = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "이젠 알아볼 수 있었다."),
        new KeyValuePair<string, string>("", "아이의 양옆으로 앉아 있는 이들은 그의 부모이다."),
        new KeyValuePair<string, string>("", "집에 손님이 올 때마다 부모는 아이에게 이 사진을 그들에게 보여주라고 시켰다."),
        new KeyValuePair<string, string>("", "허울뿐인 사진임에도 아이는 이 사진을 좋아했었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“사진 속에서는.. 날 향해 웃고계시니까…”"),
    };

    // 사진 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_PHOTO_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "5"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 인형
    public static List<KeyValuePair<string, string>> AFED_MCR_DOLL = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 리펠드 ]", "“부모님이 사주셨던 곰…”"),
        new KeyValuePair<string, string>("", "함께 외출을 하던 날 아이는 부모에게서 받았었다. 모두가 쳐다보는 앞에서."),
        new KeyValuePair<string, string>("", "아이는 벌로인해 홀로 방에 갇혀 있는 날이면 무서운 마음에 곰인형을 끌어안고 침대에 눕기 일쑤였다."),
        new KeyValuePair<string, string>("", "외로움을 느낄 때면 곰인형에게 말을 걸기도 했었다."),
    };

    // 인형 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_DOLL_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "6"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 창문
    public static List<KeyValuePair<string, string>> AFED_MCR_WINDOW = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "오랜 시간 동안 집을 관리하지 않아 뻑뻑하게 굳어 열리지 않는다."),
    };

    // 의자(위)
    public static List<KeyValuePair<string, string>> AFED_MCR_CHAIR1 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "이곳에 앉아 책을 읽었었다."),
        new KeyValuePair<string, string>("", "부모를 위해 아이는 책을 읽었다. 다른 사람들의 시선에 좋게 보이고자 책을 읽도록 시켰었다."),
        new KeyValuePair<string, string>("", "독서를 싫어하진 않았다. 하지만 무리에 가까운 양을 적은 시간 내에 읽으라는 것을 시킬 때마다 아이는 이뤄내지 못하고 벌을 받게 되었었다."),
        new KeyValuePair<string, string>("", "그 기억이 아이의 머릿속에 깊게 남아있다."),
    };

    // 의자(위) 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_CHAIR1_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "7"),
        new KeyValuePair<string, string>("value", "3"),
    };

    // 의자(아래)
    public static List<KeyValuePair<string, string>> AFED_MCR_CHAIR2 = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "부모님이 앉던 모습이 떠오른다. 아이는 눈을 질끔 감았다가 뜬다."),
        new KeyValuePair<string, string>("", "의자가 부모의 손에 높이 들려질 때면 지금처럼 눈을 감았다."),
        new KeyValuePair<string, string>("", "어떤 일이 벌어질지 두 눈으로 볼 수 없어 아이는 이렇게나마 상황을 피하고자 했다."),
        new KeyValuePair<string, string>("", "하지만 피할 순 없었다."),
        new KeyValuePair<string, string>("", "그저 고통만 늘려갈 뿐이었다."),
    };

    // 의자(아래) 우울도 값
    public static List<KeyValuePair<string, string>> AFED_MCR_CHAIR2_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "8"),
        new KeyValuePair<string, string>("value", "3"),
    };
}

// ----- 1차 엔딩 이후 거실 ----- //

public static class AFED_LR_INFO
{
    // 싱크대
    public static List<KeyValuePair<string, string>> AFED_LR_SINK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "요리를 하다 실수로 불에 손이 데인 적이 있음을 아이는 떠올린다. 데었던 손가락을 저도 모르게 쥐어잡는다."),
        new KeyValuePair<string, string>("", "괜스레 뜨거움이 느껴지는 기분이다."),
        new KeyValuePair<string, string>("", "화로를 쓰다가 손이 데여 급하게 차가운 물로 식혔던 기억이 난다."),
        new KeyValuePair<string, string>("", "아픈 와중에 부모님의 달갑지 않은 시선을 받으니 손에서 느껴지는 고통이 더 크게 느껴졌었다."),
    };

    // 싱크대 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_SINK_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "9"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 식칼/도마
    public static List<KeyValuePair<string, string>> AFED_LR_CUTTING_BOARD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "부모님이 밖으로 외출하여, 혼자 점심을 해결해야 했을 때였다."),
        new KeyValuePair<string, string>("", "아이는 엄마가 요리하던 모습을 떠올리면서 칼을 손에 쥐었다. 하지만 미숙한 탓에 피가 맺히고 말았었다."),
        new KeyValuePair<string, string>("", "부모님께 말할 수 없어 아이는 혼자 피를 휴지로 닦아냈다."),
        new KeyValuePair<string, string>("", "어떠한 말을 들을지. 어떠한 시선을 받을지. 아이는 두려움에 급히 다친 흔적을 지우고자 했다."),
    };

    // 식칼/도마 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_CUTTING_BOARD_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "10"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 화로
    public static List<KeyValuePair<string, string>> AFED_LR_BRAZIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 리펠드 ]", "“엄마가 여기서 요리를….”"),
        new KeyValuePair<string, string>("", "요리를 하던 모습이 아이의 눈앞에서 펼쳐진다."),
        new KeyValuePair<string, string>("", "날카로운 시선과 말이 자신을 향했던 기억 덧붙이듯 떠오른다."),
        new KeyValuePair<string, string>("", "얼른 접시를 놓으라는 말이 귀에 크게 꽂힌다. 그리고"),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“멍청…하게 뭘 가만히 있냐고… 들은 거 같기도 해.”"),
    };

    // 화로 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_BRAZIER_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "11"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 빵
    public static List<KeyValuePair<string, string>> AFED_LR_BREAD = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "지금과 같이 오래 방치된 빵을 아이는 먹어본 적이 있다."),
        new KeyValuePair<string, string>("", "당연히 배탈이 났었다."),
        new KeyValuePair<string, string>("", "배가 아팠을 때 아이는 부모에게 말했다. 하지만 그들은 아무런 대처도 없었다. 그저 무시에 가까웠다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“그 덕분에…. 약 위치는 알았으니까….”"),
    };

    // 빵 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_BREAD_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "12"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 부엌 의자
    public static List<KeyValuePair<string, string>> AFED_LR_CK_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "의자가 세 개. 아이와 부모. 딱 세 개가 들어맞는다. 하지만 이는 겉모습에 불과했다."),
        new KeyValuePair<string, string>("", "아이만은 그 속을 알고 있다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“같이 식사하는 날은 거의 없었지.”"),
    };

    // 부엌 의자 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_CK_CHAIR_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "13"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 화분
    public static List<KeyValuePair<string, string>> AFED_LR_FLOWERPOT = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "잎이 전부 시들었다. 아이는 자신이 손수 키웠던 식물임에 더욱 마음이 아팠다."),
        new KeyValuePair<string, string>("", "부모가 아이에게 시켜 식물을 관리해왔다. 그리고 다른 이들 앞에선 자신들이 키운 마냥 말하기 일쑤였다."),
    };

    // 화분 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_FLOWERPOT_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "14"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 책
    public static List<KeyValuePair<string, string>> AFED_LR_BOOK = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "예전에만 해도 아이는 여기까지 가까이 다가가기를 무서워했었다."),
        new KeyValuePair<string, string>("", "언제 책이 제게로 날아올지 모르기때문에 아이는 아빠의 신경을 건드리지 않기 위해 가까이 다가가는 걸 피했다."),
        new KeyValuePair<string, string>("", "책의 무게에 따라 아픔이 다르다는 걸 아이가 알게 된 계기이기도 했다."),
    };

    // 책 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_BOOK_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "15"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 시계
    public static List<KeyValuePair<string, string>> AFED_LR_WATCH = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "멈춰버린 시계는 아이의 손에 닿지 않는다."),
    };

    // 벽난로
    public static List<KeyValuePair<string, string>> AFED_LR_FIRE_PLACE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이는 이곳에 가까이 다가갈 수 없었다. 부모가 벽난로 앞에서 따뜻함을 만끽하는 동안 아이는 제 방에서 덜덜 떠는 것밖에 할 수 없었다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“가까이 다가갔다간... 무슨 일이 일어날지 몰라.”"),
    };

    // 벽난로 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_FIRE_PLACE_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "16"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 거실 의자
    public static List<KeyValuePair<string, string>> AFED_LR_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이의 부모, 엄마와 아빠가 앉는 의자이다. 아이의 자리는 없다."),
    };

    // 책장
    public static List<KeyValuePair<string, string>> AFED_LR_BOOKSHELF = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("[ 리펠드 ]", "“여긴… 제일 위험해.”"),
        new KeyValuePair<string, string>("", "실수를 저질렀던 날이었다. 주먹이 날라오던 날, 아이는 이 책장 앞에서 한껏 몸을 웅크렸었다."),
        new KeyValuePair<string, string>("", "손에 이어 발이 날아올 때쯤 위에서 책이 우르르 떨어졌다. 반동에 이기지 못한 책들이 떨어진 것이었다."),
        new KeyValuePair<string, string>("", "맞은 고통에 이어 책이 낙하하면서 실린 무게에 고통은 더해졌었다."),
        new KeyValuePair<string, string>("", "이 날 아이는 머리를 맞아 정신을 잃기까지 했었다. 아이에게 있어서 떠올리고 싶지 않은 기억이었다."),
    };

    // 책장 우울도 값
    public static List<KeyValuePair<string, string>> AFED_LR_BOOKSHELF_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "17"),
        new KeyValuePair<string, string>("value", "2"),
    };
}

// ----- 1차 엔딩 이후 부모 방 ----- //

public static class AFED_PR_INFO
{
    // 침대
    public static List<KeyValuePair<string, string>> AFED_PR_BED = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아이에게 있어서 이 침대는 가까이 다가가지 못하는 그런 곳이다."),
        new KeyValuePair<string, string>("", "멀리서 부모가 시키는 일을 해내면서 지켜보는 것이 전부였다."),
    };

    // 침대 우울도 값
    public static List<KeyValuePair<string, string>> AFED_PR_BED_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "18"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 서랍
    public static List<KeyValuePair<string, string>> AFED_PR_CHIFFONIER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "부모의 옷으로 가득하다. 서랍 안을 빼곡히 채우고 있다."),
    };

    // 거울
    public static List<KeyValuePair<string, string>> AFED_PR_MIRROR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "익숙한 얼굴이다."),
        new KeyValuePair<string, string>("[ 리펠드 ]", "“이젠 멍이 사라졌네….”"),
        new KeyValuePair<string, string>("", "아이는 부모가 잠시 밖을 나갔을 때 이 방에 조심히 들어와 거울을 확인하곤 했었다."),
        new KeyValuePair<string, string>("", "그들이 언제 들어올까 조마조마하면서도 제 얼굴을 확인했다."),
        new KeyValuePair<string, string>("", "혹여 얼굴에 멍이 있을까 봐. 상처가 있을까 봐. 만약 있다면 밖을 나갈 수 없었기에 확인을 했었다."),
    };

    // 거울 우울도 값
    public static List<KeyValuePair<string, string>> AFED_PR_MIRROR_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "19"),
        new KeyValuePair<string, string>("value", "2"),
    };

    // 행거
    public static List<KeyValuePair<string, string>> AFED_PR_HANGER = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "가까이 다가가고 싶지 않아 아이는 외면한다."),
    };

    // 테이블
    public static List<KeyValuePair<string, string>> AFED_PR_TABLE = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "아빠가 쓰던 물건임을 아이는 떠올려낸다. 무슨 내용인지는 여전히 알 수 없다."),
        new KeyValuePair<string, string>("", "아이에게 알려준 것이 없으니 모를 수밖에."),
        new KeyValuePair<string, string>("", "관심을 가지려고 할 수조차 없었기에 아이는 궁금증을 없애보려 노력했었다."),
    };

    // 테이블 우울도 값
    public static List<KeyValuePair<string, string>> AFED_PR_TABLE_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "20"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 의자
    public static List<KeyValuePair<string, string>> AFED_PR_CHAIR = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "테이블과 같이 아이의 아빠가 쓰던 의자이다."),
        new KeyValuePair<string, string>("", "이 의자도 예외는 아니었다. 문 앞에 서 있는 아이를 향해 던지기 일쑤였다."),
        new KeyValuePair<string, string>("", "안 부서진 게 신기할 만큼 큰 소리가 났었다."),
        new KeyValuePair<string, string>("", "아이는 덜덜 떨리는 마음을 심호흡으로 진정시키고자 했다."),
    };

    // 의자 우울도 값
    public static List<KeyValuePair<string, string>> AFED_PR_CHAIR_DV = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("index", "21"),
        new KeyValuePair<string, string>("value", "1"),
    };

    // 창문
    public static List<KeyValuePair<string, string>> AFED_PR_WINDOW = new List<KeyValuePair<string, string>>()
    {
        new KeyValuePair<string, string>("", "오랜 시간 방치해두어 창문이 뻑뻑하다."),
    };
}