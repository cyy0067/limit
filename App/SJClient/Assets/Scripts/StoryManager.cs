using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryManager : MonoBehaviour
{
    public string storyInfo;
 
    private StoryFactory storyFactory;
    private bool storyFlag = false;

    private void Start()
    {
        storyFactory = GameObject.FindGameObjectWithTag("Managers").GetComponent<StoryFactory>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (storyFlag) return;
        storyFlag = true;

        if (other.gameObject.CompareTag("Player"))
        {
            if (storyInfo.Equals("NPC_STORY_INFO_1"))
            {
                StartCoroutine(storyFactory.NPC_Story_1());
            }

            if (storyInfo.Equals("GHOST_STORY_INFO_1"))
            {
                StartCoroutine(storyFactory.Ghost_Story(new Vector3(80.13f, 38.29f, 0f)));
            }

            if (storyInfo.Equals("GHOST_STORY_INFO_2"))
            {
                StartCoroutine(storyFactory.Ghost_Story(new Vector3(73.38f, 39.3f, 0f)));
            }

            if (storyInfo.Equals("CAVE_EXIT_STORY_INFO"))
            {
                StartCoroutine(storyFactory.Cave_Exit_Story());
            }
            storyFlag = false;
        }
    }
}
