using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAIManager : MonoBehaviour
{
    public float moveSpeed; // 플레이어 속도
    public bool startMove = false; // 시작

    private GameObject player;
    private Transform playerTrans;
    private Rigidbody2D rb2D; // Rigidbody2D를 불러오기 위한 변수
    private Animator ghostAnim; // Animator를 불러오기 위한 변수

    private StoryFactory storyFactory;

    private bool ghostDirIsX = false;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        ghostAnim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
        storyFactory = GameObject.FindGameObjectWithTag("Managers").GetComponent<StoryFactory>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!startMove) return;

        float x_dir = playerTrans.position.x - transform.position.x;
        float y_dir = playerTrans.position.y - transform.position.y;

        ghostDirIsX = false;

        if (Mathf.Abs(x_dir) > Mathf.Abs(y_dir)) ghostDirIsX = true;

        x_dir = (x_dir < 0) ? -1f : 1f;
        y_dir = (y_dir < 0) ? -1f : 1f;

        if (ghostDirIsX)
        {
            if (x_dir == 1f)
            {
                ghostAnim.SetFloat("MoveX", 1f);
                ghostAnim.SetFloat("MoveY", 0f);
            }
            else
            {
                ghostAnim.SetFloat("MoveX", -1f);
                ghostAnim.SetFloat("MoveY", 0f);
            }

            y_dir = (y_dir < 0) ? -0.5f : 0.5f;
        }
        else
        {
            if (y_dir == 1f)
            {
                ghostAnim.SetFloat("MoveX", 0f);
                ghostAnim.SetFloat("MoveY", 1f);
            }
            else
            {
                ghostAnim.SetFloat("MoveX", 0f);
                ghostAnim.SetFloat("MoveY", -1f);
            }

            x_dir = (x_dir < 0) ? -0.5f : 0.5f;
        }

        rb2D.MovePosition(rb2D.position + new Vector2(x_dir, y_dir).normalized * moveSpeed * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D obj)
    {
        string objectName = obj.gameObject.name;

        // 플레이어와 악몽이 접촉했다.
        if (objectName.Contains("Player"))
        {
            this.startMove = false;
            StartCoroutine(storyFactory.Ghost_Catched_Story(this.gameObject));
        }
    }
}
