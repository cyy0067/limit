using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint
{
    public float X { get; }
    public float Y { get; }

    public string LimitFlag { get; }

    public SpawnPoint(float x, float y, string limitFlag = "") => (X, Y, LimitFlag) = (x, y, limitFlag);
}

public class ChangeMapManager : MonoBehaviour
{
    private List<KeyValuePair<string, SpawnPoint>> MAP_INFO = new List<KeyValuePair<string, SpawnPoint>>()
    {
        new KeyValuePair<string, SpawnPoint>("MCRToLR", new SpawnPoint(27.6f, -2f)),
        new KeyValuePair<string, SpawnPoint>("LRToMCR", new SpawnPoint(2.4f, -2f)),
        new KeyValuePair<string, SpawnPoint>("LRToPR", new SpawnPoint(60f, -2.4f)),
        new KeyValuePair<string, SpawnPoint>("PRToLR", new SpawnPoint(36f, -2f)),
        new KeyValuePair<string, SpawnPoint>("LRToOUT", new SpawnPoint(24.7f, 43.3f, "on")),
        new KeyValuePair<string, SpawnPoint>("OUTToLR", new SpawnPoint(32f, -2.3f, "off")),
        new KeyValuePair<string, SpawnPoint>("H1ToOUT", new SpawnPoint(31.35f, 31.7f, "on")),
        new KeyValuePair<string, SpawnPoint>("OUTToH1", new SpawnPoint(93.4f, -2.7f, "off")),
        new KeyValuePair<string, SpawnPoint>("H3ToOUT", new SpawnPoint(22.40f, 55.68f, "on")),
        new KeyValuePair<string, SpawnPoint>("OUTToH3", new SpawnPoint(31.48f, -30.3f, "off")),
        new KeyValuePair<string, SpawnPoint>("H6ToOUT", new SpawnPoint(45.93f, 39.36f, "on")),
        new KeyValuePair<string, SpawnPoint>("OUTToH6", new SpawnPoint(59.5f, -31.19f, "off")),
        new KeyValuePair<string, SpawnPoint>("CVToOUT", new SpawnPoint(41f, 51.2f, "on")),
        new KeyValuePair<string, SpawnPoint>("H5ToOUT", new SpawnPoint(42.3f, 55.21f, "on")),
        new KeyValuePair<string, SpawnPoint>("OUTToH5", new SpawnPoint(86.46f, -31.32f, "off")),
        new KeyValuePair<string, SpawnPoint>("[AFED]MCRToLR", new SpawnPoint(27.6f, -62f)),
        new KeyValuePair<string, SpawnPoint>("[AFED]LRToMCR", new SpawnPoint(2.4f, -62f)),
        new KeyValuePair<string, SpawnPoint>("[AFED]LRToPR", new SpawnPoint(60f, -62.4f)),
        new KeyValuePair<string, SpawnPoint>("[AFED]PRToLR", new SpawnPoint(36f, -62f)),
    };

    public string FromAndToMapInfo;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach (KeyValuePair<string, SpawnPoint> mapInfo in MAP_INFO)
            {
                if (mapInfo.Key.Equals(FromAndToMapInfo))
                {
                    StartCoroutine(ChangePosition(mapInfo.Value.X, mapInfo.Value.Y, mapInfo.Value.LimitFlag));
                    
                    GameObject ghostObj = GameObject.FindGameObjectWithTag("Ghost");
                    if (FromAndToMapInfo.Equals("CVToOUT") && ghostObj)
                    {
                        // �Ǹ� ����
                        Destroy(ghostObj);

                        // �ǵ�����
                        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider1").GetComponent<BoxCollider2D>().enabled = true;
                        GameObject.Find("/BackGround/Cave/NPC/GhostStoryCollider2").GetComponent<BoxCollider2D>().enabled = true;
                    }
                }
            }
        }
    }

    private IEnumerator ChangePosition(float x, float y, string limitFlag)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().moveStop = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().anim.SetBool("PlayerMoving", false);
        
        // fade in
        yield return StartCoroutine(GameObject.Find("Managers").GetComponent<MainSceneController>().FadeIn());
        
        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(x, y, 0f);
        if (limitFlag == "on") GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().limitFlag = true;
        if (limitFlag == "off") GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().limitFlag = false;

        // fade out
        yield return StartCoroutine(GameObject.Find("Managers").GetComponent<MainSceneController>().FadeOut());
        
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().moveStop = false;
    }
}
