using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;

public class SaveDataManager : MonoBehaviour
{
    public static SaveDataManager instance;

    public SaveData LoadedMetaData = new SaveData 
    {
        index = -1,
        player_pos_x = 0.0f,
        player_pos_y = 0.0f,
        camera_limit = false,
        happy_gauge = 0,
        depress_gauge = 0,

        memory_history_list = new List<bool>(){ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
        afed_history_list = new List<bool>() { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },

        sPrologue = false,
        sNPC1 = false,
        sNPC2 = false,
        sNPC3 = false,
        sCave = false,
        sExit = false,

        start_time = 0.0f,
        end_time = 0.0f,
        start_date = ""
    };

    public List<SaveData> SaveDataList = new List<SaveData>(); 

    void Awake() 
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void LoadToObject()
    {
        Scene scene = SceneManager.GetActiveScene();
        GameObject loadDataContent = GameObject.FindGameObjectWithTag("LoadDataContent");

        // 기존 데이터 리셋하고
        foreach (Transform trans in loadDataContent.transform)
        {
            trans.GetChild(0).Find("Cancel_Button").gameObject.SetActive(false);
            trans.GetChild(0).Find("Happy_Gauge").GetComponent<Text>().text = "";
            trans.GetChild(0).Find("Depress_Gauge").GetComponent<Text>().text = "";
            trans.GetChild(0).Find("Play_Time").GetComponent<Text>().text = "";
            trans.GetChild(0).Find("Date").GetComponent<Text>().text = "";

            // 삭제 버튼 이벤트 추가
            EventTrigger trigger = trans.GetChild(0).Find("Cancel_Button").gameObject.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            if (trigger.triggers.Count == 0)
            {
                entry.eventID = EventTriggerType.PointerUp;
                entry.callback.AddListener((eventData) => { this.OpenRemovePanel(trans); });
                trigger.triggers.Add(entry);
            }

            // 불러오기 이벤트 삭제
            trigger = trans.GetChild(0).gameObject.GetComponent<EventTrigger>();
            trigger.triggers.RemoveAll(x => true);

            // Main 씬
            if (scene.name.Equals("MainScene"))
            {
                // 저장하기 이벤트 추가
                trigger = trans.GetChild(0).gameObject.GetComponent<EventTrigger>();
                entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerUp;
                entry.callback.AddListener((eventData) => { this.OpenSavePanel(trans); });
                trigger.triggers.Add(entry);
            }
        }

        // 로드된 데이터로 채운다.
        foreach (SaveData saveData in this.SaveDataList)
        {
            Transform loadData = loadDataContent.transform.GetChild(saveData.index - 1).GetChild(0);
            string happyGauge = "행복 게이지: " + saveData.happy_gauge.ToString() + "%";
            string depressGauge = "우울 게이지: " + saveData.depress_gauge.ToString() + "%";
            string PlayTotalTime = "플레이 타임: " + FormatSeconds(saveData.end_time - saveData.start_time);
            string LastDate = saveData.start_date;

            loadData.Find("Cancel_Button").gameObject.SetActive(true);
            loadData.Find("Happy_Gauge").GetComponent<Text>().text = happyGauge;
            loadData.Find("Depress_Gauge").GetComponent<Text>().text = depressGauge;
            loadData.Find("Play_Time").GetComponent<Text>().text = PlayTotalTime;
            loadData.Find("Date").GetComponent<Text>().text = LastDate;

            // 저장하기 이벤트 삭제
            EventTrigger trigger = loadData.gameObject.GetComponent<EventTrigger>();
            trigger.triggers.RemoveAll(x => true);

            // Intro 씬
            if (scene.name.Equals("IntroScene"))
            {
                // 불러오기 이벤트 추가
                trigger = loadData.gameObject.GetComponent<EventTrigger>();
                EventTrigger.Entry entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerUp;
                entry.callback.AddListener((eventData) => { this.OpenContinuePanel(saveData); });
                trigger.triggers.Add(entry);
            }
        }
    }

    public void DeleteData(Transform trans)
    {
        Scene scene = SceneManager.GetActiveScene();

        // 오브젝트 이름으로 인덱스 정보를 가져온다.
        int data_idx = Int32.Parse(trans.name.Substring(trans.name.Length - 1, 1));

        // 오브젝트 정보 지우고
        trans.GetChild(0).Find("Cancel_Button").gameObject.SetActive(false);
        trans.GetChild(0).Find("Happy_Gauge").GetComponent<Text>().text = "";
        trans.GetChild(0).Find("Depress_Gauge").GetComponent<Text>().text = "";
        trans.GetChild(0).Find("Play_Time").GetComponent<Text>().text = "";
        trans.GetChild(0).Find("Date").GetComponent<Text>().text = "";

        // 불러오기 이벤트 삭제
        EventTrigger trigger = trans.GetChild(0).gameObject.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        trigger.triggers.RemoveAll(x => true);

        // Main 씬
        if (scene.name.Equals("MainScene"))
        {
            // 저장하기 이벤트 추가
            trigger = trans.GetChild(0).gameObject.GetComponent<EventTrigger>();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { this.OpenSavePanel(trans); });
            trigger.triggers.Add(entry);
        }

        // List에서 지우고 json으로 저장하자
        int list_idx = SaveDataList.FindIndex(data => data.index == (data_idx + 1));
        SaveDataList.RemoveAt(list_idx);
        // gameObject.GetComponent<JsonDataController>().JsonSave();

        this.CloseRemovePanel();
    }

    public void OpenRemovePanel(Transform trans)
    {
        // 버튼 이벤트 추가
        GameObject removePanel = GameObject.FindWithTag("LoadPanel").transform.Find("RemovePanel").gameObject;

        // Yes Button
        EventTrigger trigger = removePanel.transform.GetChild(0).Find("Yes Button").gameObject.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { this.DeleteData(trans); });
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        trigger = removePanel.transform.GetChild(0).Find("No Button").gameObject.GetComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { this.CloseRemovePanel(); });

        // No Button
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        // Close Button
        trigger = removePanel.transform.GetChild(0).Find("Close Button").gameObject.GetComponent<EventTrigger>();
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        removePanel.SetActive(true);
    }

    public void CloseRemovePanel()
    {
        GameObject removePanel = GameObject.FindWithTag("LoadPanel").transform.Find("RemovePanel").gameObject;
        removePanel.SetActive(false);
    }

    public void SetMetadataAndLoadScene(SaveData saveData)
    {
        // 메타데이터를 설정한다.
        LoadedMetaData = saveData;

        // 메인씬으로 전환한다.
        StartCoroutine(GameObject.Find("Managers").GetComponent<IntroController>().FadeInAndLoadMainScene());
        
        Task.Delay(500).ContinueWith((task) => { this.CloseContinuePanel(); });
    }

    public void OpenContinuePanel(SaveData saveData)
    {
        // 버튼 이벤트 추가
        GameObject continuePanel = GameObject.FindWithTag("LoadPanel").transform.Find("ContinuePanel").gameObject;

        // Yes Button
        EventTrigger trigger = continuePanel.transform.GetChild(0).Find("Yes Button").gameObject.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { this.SetMetadataAndLoadScene(saveData); });
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        trigger = continuePanel.transform.GetChild(0).Find("No Button").gameObject.GetComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => { this.CloseContinuePanel(); });

        // No Button
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        // Close Button
        trigger = continuePanel.transform.GetChild(0).Find("Close Button").gameObject.GetComponent<EventTrigger>();
        trigger.triggers.RemoveAll(x => true);
        trigger.triggers.Add(entry);

        continuePanel.SetActive(true);
    }

    public void CloseContinuePanel()
    {
        GameObject continuePanel = GameObject.FindWithTag("LoadPanel").transform.Find("ContinuePanel").gameObject;
        continuePanel.SetActive(false);
    }

    public void OpenSavePanel(Transform trans)
    {
        // 오브젝트 이름으로 인덱스 정보를 가져온다.
        int data_idx = Int32.Parse(trans.name.Substring(trans.name.Length - 1, 1));
        int list_idx = SaveDataList.FindIndex(data => data.index > data_idx + 1);

        GameObject player_obj = GameObject.FindGameObjectWithTag("Player");
        GameObject cameera_obj = GameObject.FindGameObjectWithTag("MainCamera");
        Transform player_trans = player_obj.GetComponent<Transform>();
        PlayerController playerController = player_obj.GetComponent<PlayerController>();

        GameObject loadDataContent = GameObject.FindGameObjectWithTag("LoadDataContent");

        // 데이터 리스트에 추가한다.
        SaveData saveData = new SaveData();
        saveData.index = data_idx + 1;
        saveData.player_pos_x = player_trans.position.x;
        saveData.player_pos_y = player_trans.position.y;
        saveData.camera_limit = cameera_obj.GetComponent<CameraController>().limitFlag;
        saveData.happy_gauge = playerController.happy_gauge;
        saveData.depress_gauge = playerController.depress_gauge;
        saveData.memory_history_list = playerController.memory_history_list;
        saveData.afed_history_list = playerController.afed_history_list;
        saveData.sPrologue = playerController.sPrologue;
        saveData.sNPC1 = playerController.sNPC1;
        saveData.sNPC2 = playerController.sNPC2;
        saveData.sNPC3 = playerController.sNPC3;
        saveData.sCave = playerController.sCave;
        saveData.sExit = playerController.sExit;
        saveData.start_time = playerController.startTime;
        saveData.end_time = Time.time;
        saveData.start_date = DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "/");

        // 없을 경우, 그냥 추가
        if (list_idx == -1)
            SaveDataList.Add(saveData);
        else // 있을 경우, 그 아이의 위치에 추가
            SaveDataList.Insert(list_idx, saveData);

        // gameObject.GetComponent<JsonDataController>().JsonSave();

        // UI 리스트에 추가한다.
        Transform loadData = loadDataContent.transform.GetChild(saveData.index - 1).GetChild(0);
        string happyGauge = "행복 게이지: " + saveData.happy_gauge.ToString() + "%";
        string depressGauge = "우울 게이지: " + saveData.depress_gauge.ToString() + "%";
        string PlayTotalTime = "플레이 타임: " + FormatSeconds(saveData.end_time - saveData.start_time);
        string LastDate = saveData.start_date;

        loadData.Find("Cancel_Button").gameObject.SetActive(true);
        loadData.Find("Happy_Gauge").GetComponent<Text>().text = happyGauge;
        loadData.Find("Depress_Gauge").GetComponent<Text>().text = depressGauge;
        loadData.Find("Play_Time").GetComponent<Text>().text = PlayTotalTime;
        loadData.Find("Date").GetComponent<Text>().text = LastDate;

        // 저장하기 이벤트 삭제
        EventTrigger trigger = loadData.gameObject.GetComponent<EventTrigger>();
        trigger.triggers.RemoveAll(x => true);

        // 저장 패널 소환
        StartCoroutine(this.CloseSavePanel());
    }

    string FormatSeconds(float elapsed)
    {
        int d = (int)(elapsed * 100.0f);
        int minutes = d / (60 * 100);
        int seconds = (d % (60 * 100)) / 100;
        int hundredths = d % 100;
        return String.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, hundredths);
    }

    public IEnumerator CloseSavePanel()
    {
        GameObject savePanel = GameObject.FindWithTag("LoadPanel").transform.Find("SavePanel").gameObject;
        savePanel.SetActive(true);
        byte fadeCount = 255;

        while (fadeCount > 0)
        {
            fadeCount -= 1;
            yield return new WaitForSeconds(0.01f); // 0.01초마다
            savePanel.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(240, 240, 240, fadeCount); // 해당 변수 값으로 알파 값 지정
            savePanel.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color32(50, 50, 50, fadeCount); // 해당 변수 값으로 알파 값 지정
        }

        savePanel.SetActive(false);

        // 초기화
        savePanel.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(240, 240, 240, 255); 
        savePanel.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color32(50, 50, 50, 255); 
    }
}

[Serializable]
public class SaveDataList
{
    [SerializeField] public List<SaveData> save_data_list = new List<SaveData>();
}

[Serializable]
public class SaveData
{
    [SerializeField] public int index;

    [SerializeField] public float player_pos_x;
    [SerializeField] public float player_pos_y;
    [SerializeField] public bool camera_limit;

    [SerializeField] public int happy_gauge;
    [SerializeField] public int depress_gauge;

    [SerializeField] public List<bool> memory_history_list;
    [SerializeField] public List<bool> afed_history_list;

    [SerializeField] public bool sPrologue;
    [SerializeField] public bool sNPC1;
    [SerializeField] public bool sNPC2;
    [SerializeField] public bool sNPC3;
    [SerializeField] public bool sCave;
    [SerializeField] public bool sExit;

    [SerializeField] public float start_time;
    [SerializeField] public float end_time;
    [SerializeField] public string start_date;
} 