using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int happy_gauge = 0; // 행복 게이지
    public int depress_gauge = 0; // 우울 게이지

    public List<bool> memory_history_list = new List<bool>() // 기억 정보 
    { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

    public List<bool> afed_history_list = new List<bool>() // 기억 정보 
    { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

    public bool sPrologue = false;  // 프롤로그 진행 여부
    public bool sNPC1 = false;      // NPC1 스토리 진행 여부
    public bool sNPC2 = false;      // NPC2 스토리 진행 여부
    public bool sNPC3 = false;      // NPC3 스토리 진행 여부
    public bool sCave = false;      // Cave 스토리 진행 여부
    public bool sExit = false;      // Exit 스토리 진행 여부

    public float startTime = 0.0f;  // 시작 시간 기록

    public float moveSpeed; // 플레이어 속도

    public Animator anim; // Animator를 불러오기 위한 변수
    private Rigidbody2D rb2D; // Rigidbody2D를 불러오기 위한 변수
    private bool playerMoving; // 플레이어가 움직이는 지
    private Vector2 playerMove; // 움직임이 어느 방향이었는지 확인하기 위한 변수

    public GameObject operationKeys;

    public bool inputLeft = false;
    public bool inputRight = false;
    public bool inputUp = false;
    public bool inputDown = false;

    public bool canResearch = false;
    public bool inputResearch = false;
    public List<string> canResearchObjs;

    public bool moveStop = false;

    private MainSceneController mainSceneController;
    private StoryFactory storyFactory;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        canResearchObjs = new List<String>();

        mainSceneController = GameObject.Find("Managers").GetComponent<MainSceneController>();
        storyFactory = GameObject.FindGameObjectWithTag("Managers").GetComponent<StoryFactory>();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveStop) return;

        playerMoving = false; // 따로 방향키를 누르지 않으면 플레이어는 움직이지 않음.

        float playerX = 0f;
        float playerY = 0f;

        // 왼쪽, 오른쪽 키보드
        if (Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Horizontal") < 0)
        {
            playerMove = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);
            playerMoving = true;
        }
        else if (inputLeft) // 왼쪽 버튼
        {
            playerX = -1f;
            playerMove = new Vector2(-1f, 0f);
            playerMoving = true;
        }
        else if (inputRight) // 오른쪽 버튼
        {
            playerX = 1f;
            playerMove = new Vector2(1f, 0f);
            playerMoving = true;
        }

        // 위, 아래 키보드
        if (Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Vertical") < 0)
        {
            playerMove = new Vector2(0f, Input.GetAxisRaw("Vertical")); 
            playerMoving = true;
        }
        else if (inputUp) // 위 버튼
        {
            playerY = 1f;
            playerMove = new Vector2(0f, 1f);
            playerMoving = true;
        }
        else if (inputDown) // 아래 버튼
        {
            playerY = -1f;
            playerMove = new Vector2(0f, -1f);
            playerMoving = true;
        }

        // 움직임 계산 
        if (Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Vertical") < 0)
            rb2D.MovePosition(rb2D.position + new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * moveSpeed * Time.fixedDeltaTime);
        if (inputLeft || inputRight || inputUp  || inputDown)
            rb2D.MovePosition(rb2D.position + new Vector2(playerX, playerY).normalized * moveSpeed * Time.fixedDeltaTime);

        anim.SetBool("PlayerMoving", playerMoving);

        anim.SetFloat("MoveX", playerMove.x);
        anim.SetFloat("MoveY", playerMove.y);
    }

    private void OnTriggerEnter2D(Collider2D obj)
    { 
        Type type = null;
        bool fieldCheck = false;
        string objectName = obj.gameObject.name;

        if (objectName.Contains("MCR"))         type = typeof(MCR_INFO);        // 주인공 방
        if (objectName.Contains("LR"))          type = typeof(LR_INFO);         // 거실
        if (objectName.Contains("PR"))          type = typeof(PR_INFO);         // 부모님 방
        if (objectName.Contains("VG"))          type = typeof(VG_INFO);         // 마을
        if (objectName.Contains("H1"))          type = typeof(H1_INFO);         // 집1
        if (objectName.Contains("H3"))          type = typeof(H3_INFO);         // 집3
        if (objectName.Contains("H5"))          type = typeof(H5_INFO);         // 집5
        if (objectName.Contains("H6"))          type = typeof(H6_INFO);         // 집6
        if (objectName.Contains("NPC"))         type = typeof(NPC_INFO);        // NPC
        if (objectName.Contains("AFED_MCR"))    type = typeof(AFED_MCR_INFO);   // 1차 엔딩 이후 주인공 방
        if (objectName.Contains("AFED_LR"))     type = typeof(AFED_LR_INFO);    // 1차 엔딩 이후 거실
        if (objectName.Contains("AFED_PR"))     type = typeof(AFED_PR_INFO);    // 1차 엔딩 이후 부모님 방

        // 존재하지 않는 클래스 타입
        if (type == null) return;

        foreach (var field in type.GetFields())
        {
            if (string.Equals(field.Name, objectName))
            {
                fieldCheck = true;
            }
        }

        // 존재하지 않는 클래스의 필드
        if (fieldCheck == false) return;

        // Add
        if (!canResearchObjs.Exists(o => o == objectName)) canResearchObjs.Add(objectName);
    }

    private void OnTriggerExit2D(Collider2D obj)
    {
        Type type = null;
        bool fieldCheck = false;
        string objectName = obj.gameObject.name;

        if (objectName.Contains("MCR"))         type = typeof(MCR_INFO);         // 주인공 방
        if (objectName.Contains("LR"))          type = typeof(LR_INFO);          // 거실
        if (objectName.Contains("PR"))          type = typeof(PR_INFO);          // 부모님 방
        if (objectName.Contains("VG"))          type = typeof(VG_INFO);          // 마을
        if (objectName.Contains("H1"))          type = typeof(H1_INFO);          // 집1
        if (objectName.Contains("H3"))          type = typeof(H3_INFO);          // 집3
        if (objectName.Contains("H5"))          type = typeof(H5_INFO);          // 집5
        if (objectName.Contains("H6"))          type = typeof(H6_INFO);          // 집6
        if (objectName.Contains("NPC"))         type = typeof(NPC_INFO);         // NPC
        if (objectName.Contains("AFED_MCR"))    type = typeof(AFED_MCR_INFO);    // 1차 엔딩 이후 주인공 방
        if (objectName.Contains("AFED_LR"))     type = typeof(AFED_LR_INFO);     // 1차 엔딩 이후 거실
        if (objectName.Contains("AFED_PR"))     type = typeof(AFED_PR_INFO);     // 1차 엔딩 이후 부모님 방

        // 존재하지 않는 클래스 타입
        if (type == null) return;

        foreach (var field in type.GetFields())
        {
            if (string.Equals(field.Name, objectName))
            {
                fieldCheck = true;
            }
        }

        // 존재하지 않는 클래스의 필드
        if (fieldCheck == false) return;

        // Remove
        if (canResearchObjs.Exists(o => o == objectName)) canResearchObjs.Remove(objectName);
    }

    public IEnumerator OnTriggerReSeacrch()
    {
        if (canResearchObjs.Count == 0) yield break;

        Type type = null;
        string objectName = canResearchObjs[0];

        if (objectName.Contains("MCR"))         type = typeof(MCR_INFO);         // 주인공 방
        if (objectName.Contains("LR"))          type = typeof(LR_INFO);          // 거실
        if (objectName.Contains("PR"))          type = typeof(PR_INFO);          // 부모님 방
        if (objectName.Contains("VG"))          type = typeof(VG_INFO);          // 마을
        if (objectName.Contains("H1"))          type = typeof(H1_INFO);          // 집1
        if (objectName.Contains("H3"))          type = typeof(H3_INFO);          // 집3
        if (objectName.Contains("H5"))          type = typeof(H5_INFO);          // 집5
        if (objectName.Contains("H6"))          type = typeof(H6_INFO);          // 집6
        if (objectName.Contains("NPC"))         type = typeof(NPC_INFO);         // NPC
        if (objectName.Contains("AFED_MCR"))    type = typeof(AFED_MCR_INFO);    // 1차 엔딩 이후 주인공 방
        if (objectName.Contains("AFED_LR"))     type = typeof(AFED_LR_INFO);     // 1차 엔딩 이후 거실
        if (objectName.Contains("AFED_PR"))     type = typeof(AFED_PR_INFO);     // 1차 엔딩 이후 부모님 방

        operationKeys.SetActive(false);
        anim.SetBool("PlayerMoving", false);
        inputLeft = false; inputRight = false; inputDown = false; inputUp = false;
        moveStop = true; 

        // NPC SET DIRECTION
        if (objectName.Contains("NPC"))
        {
            Animator playerAnim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
            Animator npcAnim = GameObject.FindGameObjectWithTag(objectName).GetComponent<Animator>();

            // 플레이어를 바라보게 하자
            if (playerAnim.GetFloat("MoveX") == 1f)
            {
                npcAnim.SetFloat("MoveX", -1f);
                npcAnim.SetFloat("MoveY", 0f);
            }
            else if (playerAnim.GetFloat("MoveX") == -1f)
            {
                npcAnim.SetFloat("MoveX", 1f);
                npcAnim.SetFloat("MoveY", 0f);
            }
            else if (playerAnim.GetFloat("MoveY") == -1f)
            {
                npcAnim.SetFloat("MoveX", 0f);
                npcAnim.SetFloat("MoveY", 1f);
            }
            else
            {
                npcAnim.SetFloat("MoveX", 0f);
                npcAnim.SetFloat("MoveY", -1f);
            }
        }

        // 기억이 포함된 오브젝트일 경우
        if ((objectName.Length > 2 && objectName.Substring(objectName.Length - 2).Equals("MM")))
        {
            List<KeyValuePair<string, string>> memoryInfo = (List<KeyValuePair<string, string>>)type.GetField(objectName.Substring(0, objectName.Length - 3)).GetValue(null);
            List<KeyValuePair<string, string>> diaryInfo = (List<KeyValuePair<string, string>>)type.GetField(objectName).GetValue(null);
            Int16 index = Convert.ToInt16(diaryInfo[0].Value);

            yield return StartCoroutine(mainSceneController.OnChatDialog(memoryInfo));

            // 본 기억이 없을 경우에만 진행한다.
            if (!memory_history_list[index])
            {
                // 미리 킨다. 
                operationKeys.SetActive(true);
                moveStop = false;

                yield return StartCoroutine(mainSceneController.OnDiaryDialog(diaryInfo));
                yield return StartCoroutine(mainSceneController.OnCollectDialog(diaryInfo));

                // 만약 행복게이지가 100이라면 첫번째 엔딩을 타도록 하자.
                if (this.happy_gauge == 100)
                    yield return StartCoroutine(storyFactory.The_First_Ending_Story());
            }
        }
        else // 그 외
        {
            // NPC2의 경우, NPC2 스토리 진행
            if (objectName.Contains("NPC2"))        yield return StartCoroutine(storyFactory.NPC_Story_2());

            // NPC3의 경우, NPC3 스토리 진행
            else if (objectName.Contains("NPC3"))   yield return StartCoroutine(storyFactory.NPC_Story_3());

            // 동굴의 경우, 동굴 스토리 진행
            if (objectName.Contains("CAVE"))        yield return StartCoroutine(storyFactory.CAVE_Story());

            // 그 외
            else
            {
                List<KeyValuePair<string, string>> memoryInfo = (List<KeyValuePair<string, string>>)type.GetField(objectName).GetValue(null);
                yield return StartCoroutine(mainSceneController.OnChatDialog(memoryInfo));

                if (objectName.Contains("AFED") && !object.ReferenceEquals(type.GetField(objectName + "_DV"), null))
                {
                    List<KeyValuePair<string, string>> afedInfoDic = (List<KeyValuePair<string, string>>)type.GetField(objectName + "_DV").GetValue(null);

                    Int16 index = Convert.ToInt16(afedInfoDic[0].Value);
                    Int16 gaugeValue = Convert.ToInt16(afedInfoDic[1].Value);

                    // 한번만 우울게이지를 올린다.
                    if (!afed_history_list[index])
                    {
                        // AFED 히스토리 갱신
                        afed_history_list[index] = true;

                        // 우울게이지 증가
                        this.depress_gauge = this.depress_gauge + gaugeValue > 100 ? 100 : this.depress_gauge + gaugeValue;
                        operationKeys.transform.Find("Depression Bar").GetChild(1).gameObject.GetComponent<Image>().fillAmount = this.depress_gauge / 100.0f;

                        // 만약 우울게이지가 100이라면 마지막 엔딩을 타도록 하자.
                        if (this.depress_gauge == 100)
                            yield return StartCoroutine(storyFactory.The_Last_Ending_Story());
                    }
                }
            }
        }

        // NPC RESET DIRECTION
        if (objectName.Contains("NPC"))
        {
            Animator npcAnim = GameObject.FindGameObjectWithTag(objectName).GetComponent<Animator>();

            List < KeyValuePair<string, float>> defaultDirectionInfo = (List<KeyValuePair<string, float>>)type.GetField(objectName + "_DEFAULT_DIRECTION").GetValue(null);
            npcAnim.SetFloat(defaultDirectionInfo[0].Key, defaultDirectionInfo[0].Value); // x
            npcAnim.SetFloat(defaultDirectionInfo[1].Key, defaultDirectionInfo[1].Value); // y
        }

        operationKeys.SetActive(true);
        moveStop = false;
    }
}
